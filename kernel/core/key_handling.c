#include "key_handling.h"
#include "comhand.h"
#include <core/serial.h>


#define MAC_BACKSPACE 127
#define BACKSPACE 8
#define RETURN 13
#define KEY_UP 'A'
#define KEY_DOWN 'B'
#define KEY_RIGHT 'C'
#define KEY_LEFT 'D'

int special_key_handling(char *buffer, int *count, char letter, int *size, int *cursor_pos) {
	switch (letter) {
		// carriage return
		case RETURN:
			return -1;

		// macOS backspace :P
		case MAC_BACKSPACE:
			perform_backspace(buffer, count, size, cursor_pos);
			break;

		// backspace
		case BACKSPACE:
			perform_backspace(buffer, count, size, cursor_pos);
			break;

		case KEY_UP:
			break;

		case KEY_DOWN:
			break;

		case KEY_RIGHT:
			if (*cursor_pos >= *size) {
				break;
			} else {
				serial_print("\033[C");
				(*cursor_pos)++;
				break;
			}

		case KEY_LEFT:
			if (*cursor_pos > 0) {
				serial_print("\b");
				(*cursor_pos)--;
				break;
			} else {
				break;
			}

		default:
			// prevent user from going over allocated memory (buffer overflow)
			if (*size < 98) {
				buffer[*cursor_pos] = letter;
				serial_print(&buffer[*cursor_pos]);

				(*count)++;
				(*size)++;
				(*cursor_pos)++;
				break;
			}
	}
	return 0;
}

void perform_backspace(char *buffer, int *count, int *size, int *cursor_pos) {
	// check if backspace can be performed
	if (*size > 0 && *cursor_pos > 0) {
		buffer[*cursor_pos-1] = 0;
		(*count)--;
		(*size)--;
		(*cursor_pos)--;

		serial_print("\b");
		serial_print(" ");
		serial_print("\b");
	}
}

