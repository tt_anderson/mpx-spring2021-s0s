/** \file key_handling.h
 * Processes the I/O and keystrokes of the user input within the terminal.
 * Allows the user to type into the terminal of the operating system.
 */

#ifndef _KEY_HANDLING_H
#define _KEY_HANDLING_H

/**
 * Handles the key strokes and processes special characters such as backspaces and any ascii character.
 * @param *buffer stores the keystrokes in memory
 * @param *count buffer size
 * @param letter the character at the count position of the buffer
 */
int special_key_handling(char *buffer, int *count, char letter, int *size, int *cursor_pos);

/**
 * Performs a backspace by deleting the current value in the buffer and decrements the count.
 * @param *buffer stores the keystrokes in memory
 * @param *count buffer size
 */
void perform_backspace(char *buffer, int *count, int *size, int *cursor_pos);

#endif
