#include <string.h>
#include <system.h>
#include <core/serial.h>
#include "pcb.h"
#include <modules/mpx_supt.h>
#include "MPX_R6.h"

PCB* cop;
context *global_context;
param params;

// q *com1_queue;
// com1_queue->count = 0;
// com1_queue->head = NULL;
// com1_queue->tail = NULL;

u32int *sys_call(context *registers);

/*
  Procedure..: klogv
  Description..: Kernel log messages. Sent to active
      serial device.
*/
void klogv(const char *msg)
{
  char logmsg[64] = {'\0'}, prefix[] = "klogv: ";
  strcat(logmsg, prefix);
  strcat(logmsg, msg);
  serial_println(logmsg);
}

/*
  Procedure..: kpanic
  Description..: Kernel panic. Prints an error message
      and halts.
*/
void kpanic(const char *msg)
{
  cli(); //disable interrupts
  char logmsg[64] = {'\0'}, prefix[] = "Panic: ";
  strcat(logmsg, prefix);
  strcat(logmsg, msg);
  klogv(logmsg);
  hlt(); //halt
}


/* R6 shit here */
u32int *sys_call(context *registers) {
  if (cop == NULL) {
    global_context = registers;
  } else {
    if (params.op_code == IDLE) {
      cop->stack_top = (unsigned char*) registers;
      cop->state = ready;
      insertPCB(cop);
    } else if (params.op_code == EXIT) {
      freePCB(cop);
    } else if (params.op_code == READ || params.op_code == WRITE) {
      /* IO Scheduler */
      IOCB *pointy = sys_alloc_mem(sizeof(IOCB));

      pointy->requestor_id = cop;
      pointy->dcb_ptr = NULL; // DCB from COM 1
      pointy->operation = params.op_code;
      pointy->buff_ptr = params.buffer_ptr;
      pointy->buff_count_ptr = params.count_ptr;
      pointy->next = NULL;

      // /* insert IOCB at the head */
      // if (com1_queue->head == NULL) {
      //   com1_queue->head = pointy;
      //   pointy->next = com1_queue->tail;
      //   com1_queue->tail = NULL;
      //   count++;
      // } else {
      //   com1_queue->tail = pointy;
      //   com1_queue->tail = pointy->next = NULL;
      // }


    }
    /*
     * check op_code for READ/WRITE requests and do shit
     * set process to block, dispatcher is called, new process dispatched
     */
  }

  PCB *process = get_head();
  while (process->suspend == suspended) {
    process = process->prev;
    // serial_println(process->name);
  }
  if(process != NULL) {
    cop = process;
    removePCB(process);
    cop->state = running;
    return (u32int*) cop->stack_top;
  }

  return (u32int *) global_context;
}
