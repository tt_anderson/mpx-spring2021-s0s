#include "comhand.h"
#include "modules/mpx_supt.h"
#include <string.h>
#include <stdint.h>
#include <string.h>
#include <core/io.h>
#include <core/serial.h>
// #include "pcb.h"
#include "pcbcommands.h"
#include "procsr3.h"
#include "mem_management.h"
#include "alarm.h"

#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_BLUE "\x1b[34m"

int times[10][3];
char messages[10][80];
int alarmCount = 0;

void set_alarm(char* message, int hours, int minutes, int seconds){

  if(alarmCount == 0){
    initAlarm();
  }

  if(hours >= 0 && hours <= 23){
    if(minutes >= 0 && minutes <= 59){
      if(seconds >= 0 && seconds <= 59){
          times[alarmCount][0] = hours;
          times[alarmCount][1] = minutes;
          times[alarmCount][2] = seconds;
          strcpy(messages[alarmCount],message);
          strcat(messages[alarmCount], "\n");

          alarmCount++;
      }
      else{
        serial_print(ANSI_COLOR_RED "Error! Invalid Time!" ANSI_COLOR_RESET);
      }
    }
    else{
      serial_print(ANSI_COLOR_RED "Error! Invalid Time!" ANSI_COLOR_RESET);
    }
  }
  else{
    serial_print(ANSI_COLOR_RED "Error! Invalid Time!" ANSI_COLOR_RESET);
  }
}

void checkAlarm(){
  while(1){
      klogv("fuck");
    char* message = "";
    int bufferSize, i;

    outb(0x70, 0x04);
    int hval = inb(0x71);
    int hours = (hval & 0x0F) + ((hval /16) * 10);
    if(hours < 0){
      hours += 24;
    }
    else if(hours > 24){
      hours -=24;
    }

    outb(0x70, 0x02);
    int mval = inb(0x71);
    int minutes = (mval & 0x0F) + ((mval / 16)* 10);

    outb(0x70, 0x00);
    int sval = inb(0x71);
    int seconds = (sval & 0x0F) + ((sval /16)* 10);

    // klogv();
    while(i < alarmCount){
      itoa(times[i][0], message);
      serial_print(message);
      serial_print("\n");

      if(times[i][0] < hours){
        strcpy(message, messages[i]);
        bufferSize = strlen(message) + 1;
        sys_req(WRITE, DEFAULT_DEVICE, message, &bufferSize);
        delete_alarm(i);
      }
      else if(times[i][0] == hours && times[i][1] == minutes && times[i][2] <= seconds){
        strcpy(message, messages[i]);
        bufferSize = strlen(message) + 1;
        sys_req(WRITE, DEFAULT_DEVICE, message, &bufferSize);
        delete_alarm(i);
      }
      else if(times[i][0] == hours && times[i][1] == minutes && times[i][2] <= seconds){
        strcpy(message, messages[i]);
        bufferSize = strlen(message) + 1;
        sys_req(WRITE, DEFAULT_DEVICE, message, &bufferSize);
        delete_alarm(i);
      }
      i++;
    }

    if(alarmCount == 0){
      sys_req(EXIT, DEFAULT_DEVICE, NULL, NULL);
    }
    else{
      sys_req(IDLE, DEFAULT_DEVICE, NULL, NULL);
    }
  }
}

void delete_alarm(int id){
  int i;
  for(i = id + 1; i < alarmCount; i++){
    strcpy(messages[i-1], messages[i]);
    times[i][0] = times[i-1][0];
    times[i][1] = times[i-1][1];
    times[i][2] = times[i-1][2];
  }
  alarmCount--;
}

int initAlarm(){
  PCB *alarm = load_proc("ALARM", &checkAlarm);

  alarm->priority = 6;
  alarm->state = 1;
  alarm->clas = 1;


  insertPCB(alarm);
  return 0;
}
