#ifndef _PCBCMDS_H
#define _PCBCMDS_H
/** \file pcbcommands.h
 * Creates the interaction and user commands to acess the functionality of the process control blocks. Displays all the necessary information regarding the PCB system within the operating system.
 */

/**
 Creates a process control block
 @param *name the identifying name of the pcb
 @param class the type of pcb, either an application or system process
 @param priority the priority assigned to the process
 */
int createPCB(char *name, int classs, int priority);

/**
 Deletes the given process control block from the respected queue
 @param *name the name of the process control block
 */
int deletePCB(char *name);

/**
 Finds the given process control block and sets its state to blocked and reinserts it into the appropriate queue
 @param *name the name of the process control block
 */
int blockPCB(char *name);

/**
 Finds the given process control block and sets its state to ready and reinserts it into the appropriate queue
 @param *name the name of the process control block
 */
int unblockPCB(char *name);

/**
 Places the process control block in the suspended state and reinserts it into the appropriate queue
 @param *name the name of the process control block
 */
int suspendPCB(char *name);

/**
 Places the process control block in the not suspended state and reinserts it into the appropriate queue
 @param *name the name of the process control block
 */
int resumePCB(char *name);

/**
 Sets a process control blocks priority and reinserts the process into the appropriate queue
 @param *name the name of the process control block
 @param priority the priority you want to assign the pcb
 */
int setPCBpriority(char *name, int priority);

/**
 Displays the process name, class, state, suspended status, and priority of the process control block
 @param *name the name of the process control block
 */
int showPCB(char *name);

/**
 Displays the process name, class, state, suspended status, and priority of the PCBs in the ready queue
 */
void showReady();

/**
 Displays the process name, class, state, suspended status, and priority of the PCBs in the blocked queue
 */
void showBlocked();

/**
 Displays the process name, class, state, suspended status, and priority of the PCBs in the ready queue and the blocked queue
 */
void showAll();

void clearQueues();

#endif
