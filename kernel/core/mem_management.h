/** \file mem_management.h
 * Creates the the data structures and necessary processes to implement a memory management system.
 * Memory is created by using two control blocks and fitted using the First Fit algorithmn.
 */

#ifndef _MEM_MANAGE_H
#define _MEM_MANAGE_H

//types
#define free 0
#define allocated 1

/**
 * Complete memory control block (CMCB)
 * Contains a starting adress and the necessary components to define a block of memory.
 * @param type whether the memory block is of type free or allocated
 * @param size the size of the memory that is being allocated by the block
 * @param start_address the starting adress used for referencing the block
 * @param name the name of the CMCB block
 */
typedef struct CMCB {
  int type;
  u32int size;
  u32int start_address;
  //char name[99];
  struct CMCB *next_cmcb;
  struct CMCB *prev_cmcb;
  struct LMCB *lmcb_pointy;
} CMCB;

/**
 * Limited memory control block (LMCB)
 * Contains a size and type variable used to reference for memory manipulation
 * @param type whether the memory block is of type free or allocated
 * @param size the size of the memory that is being allocated by the block
 */
typedef struct LMCB {
  int type;
  u32int size;
} LMCB;

/**
 * Creates a list of CMCBs, linking them together.
 * @param *head the pointer to the head of a list of CMCBs
 */
typedef struct list {
  CMCB *head;
} list;

/**
 * Initializes all of the memory in the heap
 * @param size the size of the memory you wish to intialize the heap with
 */
void init_heap(int size);

/**
 * Allocates a block of memory in the heap
 * @param size the size of the memory you wish to allocate
 */
u32int allocate_memory(int size);

/**
 * Frees an allocated block of memory in the heap
 * @param addr the address of the memory block
 */
u32int free_memory(u32int address);

/**
 * Checks to see if there is no allocated memory currently
 */
int is_empty();

/**
 * show all of the free or allocated memory list
 * types
 * 0 -> allocated_list
 * 1 -> free_list
 */
void show_list(int type);

void grab_info(u32int address);

#endif
