#include "modules/mpx_supt.h"
#include "MPX_R6.h"
#include <core/io.h>
#include <core/serial.h>
#include <core/tables.h>

/* dcb pointer */
DCB dcb_pointy;
int og_i_vector;

int com_open(int *eflag, int baud_rate) {

  /* check if eflag is valid */
  if (eflag == NULL)
    return INVALID_FLAG_PTR;

  /* can't devide by zero */
  if (baud_rate <= 0)
    return INVALID_BR_DIV;

  /* check if port is open */
  if (dcb_pointy.port_status == OPEN)
    return PORT_ALREADY_OPEN;

  /* initialize DCB */
  dcb_pointy.port_status = OPEN;
  dcb_pointy.ae_flag_ptr = eflag;
  dcb_pointy.status = F_IDLE;
  dcb_pointy.ring_buff_in = 0;
  dcb_pointy.ring_buff_out = 0;
  dcb_pointy.ring_buff_count = 0;

  /*
   * save address of current interrupt handler
   * set new interrupt handler address into interupt vector
   */
  cli();
  idt_set_gate(0x24, (u32int) top_handle, 0x08, 0x8E);

  /* calculate baud_rate */
  long baud_rate_div = 115200 / (long) baud_rate;

  /*
   * store 0x80 in line control register
   * store baud_rate_div LSB and MSB in BRD_LSB/BRD_MSB
   * store 0x03 in the line control register
   */
  outb(LINE_CONTROL_REG, 0x80);
  outb(BRD_LSB, baud_rate_div & 1);
  outb(BRD_MSB, baud_rate_div >> 8); // MSB?
  outb(LINE_CONTROL_REG, 0x03);

  /*
   * enable appropriate level?? in PIC mask register
   * eanble overall serial port interrupts by storing value 0x08 in MCR
   */
  int mask;
  mask = inb(PIC_MASK);
  mask = mask & ~(1 << 4); //help here please!
  outb(PIC_MASK, mask);
  outb(MODEM_CONTROL_REG, 0x08);
  outb(INTERRUPT_ENABLE, 0x01);
  sti();

  return 0;
}

int com_close(void) {
  /* check if port is currently open */
  if (dcb_pointy.port_status != OPEN)
    return SERIAL_PORT_NOT_OPEN;

  /* close it! */
  dcb_pointy.port_status = CLOSED;

  /* disable PIC_MASK register */
  int mask;
  disable();
  mask = inb(PIC_MASK);
  mask = mask | (1 << 4); //help here please!
  outb(PIC_MASK, mask);

  /* Clear Modem Control and Interrupt Enable Register */
  outb(MODEM_CONTROL_REG, 0x00);
  outb(INTERRUPT_ENABLE, 0x00);

  /* restore the original saved interrupt */
  //outb(COM1, og_i_vector);

  return 0;
}

int com_read(char *buf_p, int *count_p) {
  if (dcb_pointy.port_status != OPEN)
    return READ_PORT_NOT_OPEN;

  if (dcb_pointy.status != IDLE)
    return READ_DEVICE_BUSY;

  if (buf_p == NULL)
    return READ_BUFF_ADDR_INVALID;

  if (count_p == NULL)
    return READ_COUNT_INVALID;

  /* initialize input buffer variables and set status to reading */
  dcb_pointy.in_buff = buf_p;
  dcb_pointy.in_count = count_p;
  dcb_pointy.transfer_count = 0;

  /* clear event flag */
  dcb_pointy.ae_flag_ptr = CLEAR;
  dcb_pointy.status = F_READ;

  /* disable interrupts when ring buffer is accessed */
  cli();

  /*
   * copy characters from ring buffer to requestor's buffer,
   * until ring buffer is empty, request has been reached, or
   * a CR (ENTER) code has been found
   * remove from ring buffer when copied over
   *
   * enable our interrupts after exiting of loop
   */
   while ((dcb_pointy.ring_buff_count > 0) && (*(dcb_pointy.in_buff-1) != '\r')
        && (dcb_pointy.transfer_count >= *(dcb_pointy.in_count))) {

        *dcb_pointy.in_buff = dcb_pointy.ring_buffer[dcb_pointy.ring_buff_out];
        dcb_pointy.transfer_count++;
        dcb_pointy.in_buff++;
        dcb_pointy.ring_buff_out = (dcb_pointy.ring_buff_out+1); //out index?
        dcb_pointy.ring_buff_count--;

   } sti();


   /* check if more characters are still needed */
   if (dcb_pointy.transfer_count < *dcb_pointy.in_count)
      return 0;

   /* check if last character is a carriage return */
   if (*(dcb_pointy.in_buff-1) == '\r')
      *(dcb_pointy.in_buff-1) = '\0'; // null terminate
   else
      *dcb_pointy.in_buff = '\0'; // null terminate

   /* set status to idle, ae flag, and return count to requestor var */
   dcb_pointy.status = IDLE;
   *dcb_pointy.ae_flag_ptr = SET;
   *dcb_pointy.in_count = dcb_pointy.transfer_count;

   return 0;
}

int com_write(char *buf_p, int *count_p) {
  if (dcb_pointy.port_status != NULL)
    return WRITE_PORT_NOT_OPEN;

  if (dcb_pointy.status != IDLE)
    return WRITE_DEVICE_BUSY;

  if (buf_p == NULL)
    return WRITE_BUFF_ADDR_INVALID;

  if (count_p == NULL)
    return WRITE_COUNT_INVALID;

  /* install buffer pointer and counters in DCB */
  dcb_pointy.out_buff = buf_p;
  dcb_pointy.out_count = count_p;
  dcb_pointy.transfer_count = 0;

  /* set status to writing and clear flag*/
  dcb_pointy.status = F_WRITE;
  dcb_pointy.ae_flag_ptr = CLEAR;

  /* grab first character from requestor's buffer and store it in output register */
  enable();
  outb(BASE, *dcb_pointy.out_buff);
  dcb_pointy.out_buff++;
  dcb_pointy.transfer_count++;


  return 0;
}

/*
 * top level handler
 * at beginning, disable with cli()
 * enable at end with sti()
*/
void top_handle() {
  cli();
  /* do some stuff */
  sti();
}

/* disable interrupts */
void disable() {
  outb(COM1+1, inb(COM1+1) & ~(0x02));
}

/* enable interrupts */
void enable() {
  outb(COM1+1, inb(COM1+1) | (0x02));
}
