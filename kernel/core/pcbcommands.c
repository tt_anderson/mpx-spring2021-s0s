#include "pcbcommands.h"
#include "pcb.h"
#include "modules/mpx_supt.h"
#include <string.h>
#include "comhand.h"

queue *readyQueue;
queue *blockedQueue;

int createPCB(char *name, int classs, int priority){
	if(findPCB(name) != NULL){
		char *emsg = "Error: Process name already in use";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	if(priority > 9 || priority < 0){
		char *emsg = "Error: Invalid priority, must be integer 0-9";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	if(classs > 2 || classs < 0){
		char *emsg = "Error: Invalid class, must be System or Application";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	PCB *newprocess = setupPCB(name,classs,priority);
	insertPCB(newprocess);
	return 1;
}

int deletePCB(char *name){
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	PCB *process = findPCB(name);
	removePCB(process);
	freePCB(process);
	return 1;
}

int blockPCB(char *name){
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
    	int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	PCB *process = findPCB(name);
	removePCB(process);
	process->state = blocked;
	insertPCB(process);
	return 1;
}

int unblockPCB(char *name){
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
	    int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
		}
	PCB *process = findPCB(name);
	removePCB(process);
	process->state = ready;
	insertPCB(process);
	return 1;
}

int suspendPCB(char *name){
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
		int len = strlen(emsg);
	    sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	PCB *process = findPCB(name);
	process->suspend = suspended;
	return 1;
}

int resumePCB(char *name){
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	PCB *process = findPCB(name);
	process->suspend = unsuspended;
	return 1;
}

int setPCBpriority(char *name, int priority){
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	else if(priority > 9 || priority < 0){
		char *emsg = "Error: Invalid priority, must be integer 0-9";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
		}
	PCB *process = findPCB(name);
	removePCB(process);
	process->priority = priority;
	insertPCB(process);
	return 1;
}

int showPCB(char *name){
	int newline = 3;
	if(findPCB(name) == NULL){
		char *emsg = "Error: Invalid name, process not found";
		int len = strlen(emsg);
		sys_req(WRITE,DEFAULT_DEVICE,emsg,&len);
		return 0;
	}
	PCB *process = findPCB(name);

	sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	char *str = "Process Name: ";
	int slen = strlen(str);
	int nlen = strlen(process->name);
	sys_req(WRITE,DEFAULT_DEVICE,str,&slen);
	sys_req(WRITE,DEFAULT_DEVICE,process->name,&nlen);
	sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);

	char *str1 = "State: ";
	int slen1 = strlen(str1);
	int msglen;
	sys_req(WRITE,DEFAULT_DEVICE,str1,&slen1);
	if(process->state == ready){
		msglen = strlen("ready");
		sys_req(WRITE,DEFAULT_DEVICE,"Ready",&msglen);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	}else if(process->state == blocked){
		msglen = strlen("Blocked");
		sys_req(WRITE,DEFAULT_DEVICE,"Blocked",&msglen);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	}else{
		msglen = strlen("Running");
		sys_req(WRITE,DEFAULT_DEVICE,"Running",&msglen);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
    }

	char *str2 = "Class: ";
	int slen2 = strlen(str2);
	int msglen2;
	sys_req(WRITE,DEFAULT_DEVICE,str2,&slen2);
	if((process->clas) == SYS){
		msglen2 = strlen("System");
		sys_req(WRITE,DEFAULT_DEVICE,"System",&msglen2);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	}else{
		msglen2 = strlen("Application");
		sys_req(WRITE,DEFAULT_DEVICE,"Application",&msglen2);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	}

	char *str3 = "Suspend State: ";
	int slen3 = strlen(str3);
	int msglen3;
	sys_req(WRITE,DEFAULT_DEVICE,str3,&slen3);
	if(process->suspend == suspended){
		msglen3 = strlen("Suspended");
		sys_req(WRITE,DEFAULT_DEVICE,"Suspended",&msglen3);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	}else{
		msglen3 = strlen("Not suspended");
		sys_req(WRITE,DEFAULT_DEVICE,"Not suspended",&msglen3);
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);
	}

	char *str4 = "Priority: ";
	int slen4 = strlen(str4);
	sys_req(WRITE,DEFAULT_DEVICE,str4,&slen4);

	int p = process->priority;
	char buf[2];
	itoa(p,buf);
	int msglen4 = strlen(buf);
	sys_req(WRITE,DEFAULT_DEVICE,buf,&msglen4);
	sys_req(WRITE,DEFAULT_DEVICE,"\n",&newline);

	return 1;
}

void showReady(){
	char *rq = "\n---Ready Queue---";
	int rlen = strlen(rq);
	sys_req(WRITE,DEFAULT_DEVICE,rq,&rlen);

	int i = 0;
	int newl = 3;
	PCB *test =readyQueue->head;
	while(i<readyQueue->count){
		showPCB(test->name);
		if(test->prev != NULL){
			test = test->prev;
		}
		i++;
		}
	sys_req(WRITE,DEFAULT_DEVICE,"\n",&newl);
}

void showBlocked(){
	char *bq = "\n---Blocked Queue---";
	int blen = strlen(bq);
	sys_req(WRITE,DEFAULT_DEVICE,bq,&blen);

	int i = 0;
	int newl = 3;
	PCB *test = blockedQueue->head;
	while(i<blockedQueue->count){
		showPCB(test->name);
		if(test->prev != NULL){
			test = test->prev;
		}
				i++;
		}
		sys_req(WRITE,DEFAULT_DEVICE,"\n",&newl);
}

void showAll(){
	showReady();
	showBlocked();
}

void clearQueues(){
	int i = 0;
	PCB *test =readyQueue->head;
	while(i<readyQueue->count){
		deletePCB(test->name);
		if(test->prev != NULL){
			test = test->prev;
		}
		i++;
	}

	int x = 0;
	PCB *test1 = blockedQueue->head;
	while(x<blockedQueue->count){
		deletePCB(test1->name);
		if(test1->prev != NULL){
			test1 = test1->prev;
		}
				x++;
		}
}
