/** \file alarm.h
 * Processes the alarm function.
 */

#ifndef _ALARM_H
#define _ALARM_H

#include "pcb.h"

/**
 * Sets the alarm.
 * @param message the message you want to enter.
 * @param hours the hours for the timer.
 * @param minutes the minutes for the timer.
 * @param seconds the seconds for the timer.
 */
void set_alarm(char* message, int hours, int minutes, int seconds);

/**
 * Runs the processes and checks the times against the list of times.
 */
void checkAlarm();

/**
 * Deletes the alarm.
 * @param id the alarm you want to delete.
 */
void delete_alarm(int id);

/**
 * Intializes the alaarm process.
 */
int initAlarm();

#endif
