#ifndef _PCB_H
#define _PCB_H
/** \file pcb.h
 * Handles the functionality of the process control blocks. Creates the necessary structures and queues to handle and keep track of processes.
 */

//class
#define SYS 1
#define APP 0
//states
#define ready 0
#define running 1
#define blocked 2
//suspend states
#define unsuspended 0
#define suspended 1

#define stacksize 1024

/**
 The Process Control Block. Keeps track of processes in our multiprocessing system.
 @param priority the level of priority the process contains
 @param name the name of the process block
 @param clas identifies the process as either an application or system process
 @param state controls whether the process is ready, running, or blocked
 @param suspend controls whether the process is suspended or not suspended
 @param stack contains 1024 bytes of data for the process
 */
typedef struct PCB{
	int priority;
	char name[99];
	int clas;
	int state;
	int suspend;
	unsigned char stack[1024];
	unsigned char *stack_top;
	unsigned char *stack_base;
	//pointer to other pcbs
	struct PCB *next;
	struct PCB *prev;
	char *msg;
	char *time;
} PCB;

/**
 Structure for the ready and blocked queue. Holds all of the PCBs in the in the ready and blocked states.
 @param count keeps track of the amount of ready and blocked states
 */
typedef struct queue{
	int count;
	PCB *head;
	PCB *tail;
} queue;


/**
 Allocates memory for the new process control block
 */
PCB* allocatePCB();

int allocateQueues();

/**
 Tests the process control block function
 */
void test(queue *Queue);

/**
 Frees all associated memory with a process control block
 @param *pcb the pointer of the pcb
 */
int freePCB(PCB *pcb);

/**
 Initializes a process control block, and sets the defauly state to ready.
 @param *name the name of the process control block
 @param class the type of process control block, either application or system process
 @param priority the designated process priority
 */
PCB* setupPCB(char *name,int class,int priority);

/**
 Inserts a process control block into an appropriate queue
 @param *pcb the pointer of the process control block
 */
void insertPCB(PCB *pcb);

/**
 Searches all queues for a given process control block
 @param *processname the name of the process control block
 */
PCB* findPCB(char *processname);

/**
 Removes the process control block from the queue in which it is stored
 @param *pcb the pointer to the process control block
 */
int removePCB(PCB *pcb);

PCB *get_head();
#endif
