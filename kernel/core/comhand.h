/** \mainpage Introduction
 * SOS is an operating system created from the ground up using MPX. The operating system is created using the C
 * programming language and built within a unix enviroment.
 */

/** \file comhand.h
 * Processes the terminal commands for the operating system.
 */
#ifndef _COMHAND_H
#define _COMHAND_H

#include "pcb.h"


/**
 * Gets the current version of the operating system.
 */
void version();

/**
 * Returns an invalid command error when the command is not recognized.
 */

 /**
  * Prints all of the current commands and how to use them.
  */
void helplist();

/**
 * Prints an error message when something goes wrong.
 */
void print_error();

/**
 * Provides instructions on how to use each command.
 * @param command the current entry in the terminal
 */
void help(char command[]);

/**
 * Called during execuation and enables the operating system to read all of the available commands.
 */
void comhand();

/**
 * Shuts down the operating system.
 */
int shutdown();

/**
 Helper function that separates user input into their respected parameters by splitting on the delimiters.
 @param arg the user input
 @param delimiter the delimiters, eg. " "
 */
char * parse_argument(char arg[], char delimiter[]);

/**
 * Checks the current entry if has extra arguments associated with it.
 * e.g shutdown --help with help being the extra argument.
 * @param cmdBuffer takes current entry buffer from user.
 * @param bufferSize the max size of buffer. Default 100 size.
 */
int grab_or_check_args(char cmdBuffer[], int bufferSize);

/**
 * Checks the size of the current entry in the buffer.
 * @param cmdBuffer takes current entry buffer from user.
 * @param bufferSize the max size of buffer. Default 100 size.
 */
int cmd_size (char cmdBuffer[], int bufferSize);

/**
 * Retrieves the current date of the operating system.
 */
void getdate();

/**
 * Sets the date of the operating system.
 * @param year the year
 * @param month the month
 * @param day the day
 */
void setdate(int year, int month, int day);

/**
 * Sets the time of the operating system
 * @param hours the hour
 * @param minutes the minute
 * @param seconds the second
 */
void settime(int hours, int minutes, int seconds);

/**
 * Retrieves the current time of the operating system.
 */
void gettime();

/**
 * Retrieves the current year of the operating system.
 */
int getyear();

/**
 * Sets the current year of the operating system.
 * @param year the year you want to set
 */
void setyear(int year);

/**
 * Gets the current month of the operating system.
 */
int getmonth();

/**
 * Sets the current month of the operating system.
 * @param month the month you want to set
 */
void setmonth(int month);

/**
 * Gets the current day of the operating system.
 */
int getday();

/**
 * Sets the current day of the operating system.
 * @param day the day you want to set
 */
void setday(int day);

/**
 * Gets the current hour of the operating system.
 */
int gethours();

/**
 * Sets the current hour of the operating system.
 * @param hour the hour you want to set
 */
void sethours(int hour);

/**
 * Gets the current minute of the operating system.
 */
int getmins();

/**
 * Sets the current minute of the operating system.
 * @param min minute you want to set
 */
void setmin(int min);

/**
 * Gets the current second of the operating system.
 */
int getseconds();

/**
 * Sets the current second of the operating system.
 * @param seconds the second you want to set
 */
void setsec(int seconds);

/**
 * Binary coded digit converter. Converts the time to the BCD format.
 * @param time time of the operating system
 */
int BCDconvert(unsigned char time);

/**
 * Integer to binary coded digit. Converts an integer to the corresponding BCD format.
 * @param time time of the operating system
 */
int intToBCD(int time);

/**
 * Integer to string function. Converts an integer value into a character array or "string"
 * @param number the integer value
 * @param buffer the output character arrayo or "string"
 */
char *itoa(int number,char buffer[]);

/**
 * Reverses a character array.
 * @param input the character array that will be reversed
 */
void reverse(char input[]);

/**
 Yields the current running process.
 */
void yield();

/**
 Creates a process and adds it to the ready queue.
 @param *name the name of the process.
 @param *func the code to be executed.
 */
PCB *load_proc(char *name, void (*func)(void));

/**
 Loads the processes associated with testing the R3 module.
 */
void loadr3();

/**
 Begins idling the command handler process.
 */
void start_idle();

/**
 Creates and inserts an alarm process into the ready queue
 @param *message the notification that the alarm will send at the specified time
 @param *time the selected time that the alarm will trigger
 */
void insert_alarm(char *message, char *time);

/**
 Checks the list of alarm times and triggers an alarm if the individual alarm has expired.
 */
void check_alarm();

/**
  Checks to see if there is any allocated memory
*/
void check_mem();

/**
 Clears the terminal and sets input to the top of the screen.
 */
void clear();

/**
 Runs the infinite process command.
 */
void start_infinite();

#endif
