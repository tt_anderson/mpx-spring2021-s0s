/** \file MPX_R6.h
 * Module 6 Implementation. Creates I/O driven interactions.
 */

#include "pcb.h"

#ifndef _MPX_R6_H
#define _MPX_R6_H

#define PIC_MASK 0x21

// COM1 base address
#define BASE 0x03F8

#define BRD_LSB BASE
#define BRD_MSB BASE+1
#define INTERRUPT_ENABLE BASE+1
#define LINE_CONTROL_REG BASE+3
#define MODEM_CONTROL_REG BASE+4

#define OPEN 1
#define CLOSED 0
#define F_IDLE 0
#define F_READ 1
#define F_WRITE 2
#define CLEAR 0
#define SET 1

// com_open error codes
#define INVALID_FLAG_PTR -101
#define INVALID_BR_DIV -102
#define PORT_ALREADY_OPEN -103

// com_close error codes
#define SERIAL_PORT_NOT_OPEN -210

#define READ_PORT_NOT_OPEN -301
#define READ_COUNT_INVALID -302
#define READ_BUFF_ADDR_INVALID -303
#define READ_DEVICE_BUSY -304

#define WRITE_PORT_NOT_OPEN -401
#define WRITE_BUFF_ADDR_INVALID -402
#define WRITE_COUNT_INVALID -403
#define WRITE_DEVICE_BUSY -404


/**
 The device control block. Houses information about the current requested I/O device.
 @param port_status whether or not the device is opened or closed.
 @param ae_flag_ptr Signaling
 @param status Whether it is a READ, WRITE, or IDLE request.
 @param transfer_count Checks if operation is complete.
 @param in_count The amount of characters the request has in its buffer.
 @param in_buff In character.
 @param out_count The amount of characters being written to its buffer.
 @param out_buff Out character.
 @param ring_buffer Houses the leftover characters.
 @param ring_buff_in Incoming characters in the ring buffer.
 @param ring_buff_out Outgoing characters in the ring buffer.
 @param ring_buff_count The amount of characters in the ring buffer.
 */
typedef struct device {
  int  port_status;
  int  *ae_flag_ptr;
  int  status; // IDLE, READ, WRITE
  int  transfer_count; //check if operation is complete
  int  *in_count;
  char *in_buff;
  int  *out_count;
  char *out_buff;
  char ring_buffer[256];
  int  ring_buff_in;
  int  ring_buff_out;
  int  ring_buff_count;
} DCB;

/* unsure if this is all the members of the struct we need */
/**
 Houses information about the requested device.
 @param requester_id A process
 @param dcb_ptr Points to the device control block
 @param operation Whether to read or write.
 @param buff_ptr Points to the io buffer.
 @param buff_count_ptr Count of the io buffer.
 */
typedef struct io {
  PCB  *requestor_id;
  DCB  *dcb_ptr;
  int  operation; // Read || Write
  char  *buff_ptr;
  int  *buff_count_ptr;
  struct io *next;
} IOCB;

/**
 HOLDS THE IOCBSSS
 @param count the amount of IOCBS
 */
typedef struct q{
  int count;
  IOCB *head;
  IOCB *tail;
} q;

// multiple queues would be best, queue for each device

/**
 * initialize DCB
 * set new interupt hanlder address
 * computer & store baud rate divisor -> baud_rate_div = 115200 / (long) baud_rate
 * set other line characteristics
 * enable all of necessary interrupts
 *
 */
int com_open(int *eflag, int baud_rate);

/**
 * clear open indicator of DCB
 * disable level of PIC mask register
 *
 */
int com_close(void);

/**
 * obtains input characters from port and loads into requestor buffer
 *      - input characters obtained from ring buffer (if any pending)
 *      - if not, device status changed to reading
 *      - notifies interrupt handler to place characters into requestor's buffer rather
 *        than ring buffer.
 *
 * example for ring buffer: keyboard input is given but a process has not requested anything,
 *                          from the keyboard, place in ring buffer, so it's not wasted
 */
int com_read(char *buf_p, int *count_p);

/**
 * initiate transfer of a block data to serial port
 *      - get the first character from requestor's buffer
 *          - store into output register
 *      - enable write interrupts
 *      - let write interrupt handler do the rest
 *
 *
 */
int com_write(char *buf_p, int *count_p);

/*
 * IO Scheduler
 * ------------
 * process input/output requests
 * examine and ensure syscall params are valid (operation must be read/write, device legal, and buffer not null)
 * check status of requested device - check if device is available/not process using it
 *     - place all information in IOCB (if available)
 *     - insert in write/read queue (if busy)
 * return sys_call (invokes dispatched)
 */

 /*
  * Serial Port Driver - Interrupt Handler
  * -----------
  * first level determines exact cause of interrupt (read interrupt id register)
  * second level determines what needs to be called -> see slides for more detail
  *
  * Steps:
  *     - Read Interrupt ID register
  *        - determine the cause of the interrupt
  *     - Call appropriate second level handler
  *     - Clear the interrupt by sending EOI to PIC
  *
  */

  /* make functions for first level and second level handler */
void top_handle();

// enable | disable interrupts
void disable();
void enable();

#endif
