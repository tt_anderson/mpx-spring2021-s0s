/*
  ----- kmain.c -----

  Description..: Kernel main. The first function called after
      the bootloader. Initialization of hardware, system
      structures, devices, and initial processes happens here.

      Initial Kernel -- by Forrest Desjardin, 2013,
      Modifications by:    Andrew Duncan 2014,  John Jacko 2017
      				Ben Smith 2018, and Alex Wilson 2019
*/

#include <stdint.h>
#include <string.h>
#include <system.h>

#include <core/io.h>
#include <core/serial.h>
#include <core/tables.h>
#include <core/interrupts.h>
#include <mem/heap.h>
#include <mem/paging.h>

#include "modules/mpx_supt.h"
#include "comhand.h"
#include "mem_management.h"

#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_BLUE "\x1b[34m"


void kmain(void)
{
   extern uint32_t magic;
   // Uncomment if you want to access the multiboot header
   // extern void *mbd;
   // char *boot_loader_name = (char*)((long*)mbd)[16];


   // 0) Initialize Serial I/O
   // functions to initialize serial I/O can be found in serial.c
   // there are 3 functions to call



   klogv(ANSI_COLOR_CYAN "Starting MPX boot sequence..." ANSI_COLOR_RESET);
   klogv("Initialized serial I/O on COM1 device...");

   // 1) Initialize the support software by identifying the current
   //     MPX Module.  This will change with each module.
   // you will need to call mpx_init from the mpx_supt.c

   init_serial(COM1);
   set_serial_out(COM1);
   set_serial_in(COM1);
   mpx_init(MEM_MODULE);

   // 2) Check that the boot was successful and correct when using grub
   // Comment this when booting the kernel directly using QEMU, etc.
   if ( magic != 0x2BADB002 ){
     //kpanic("Boot was not error free. Halting.");
   }

   // 3) Descriptor Tables -- tables.c
   //  you will need to initialize the global
   // this keeps track of allocated segments and pages
   klogv(ANSI_COLOR_YELLOW "Initializing descriptor tables..." ANSI_COLOR_RESET);

   // Initializes the Global DescriptorTable
   init_gdt();

   // Initializes the interrupt descriptor table
   init_idt();

   // 4)  Interrupt vector table --  tables.c
   // this creates and initializes a default interrupt vector table
   // this function is in tables.c

   // Initializes the progrmamable iterrupt controllers
   init_pic();

   // Installs the initial interrupt handlers for the first 32 IRQs && enables interrupts sti()
   init_irq();

   sti();

   klogv(ANSI_COLOR_GREEN "Interrupt vector table initialized!" ANSI_COLOR_RESET);

   // 5) Virtual Memory -- paging.c  -- init_paging
   //  this function creates the kernel's heap
   //  from which memory will be allocated when the program calls
   // sys_alloc_mem UNTIL the memory management module  is completed
   // this allocates memory using discrete "pages" of physical memory
   // NOTE:  You will only have about 70000 bytes of dynamic memory
   //
   klogv(ANSI_COLOR_YELLOW "Initializing virtual memory..." ANSI_COLOR_RESET);

   init_paging();

   //initialize process queues
   allocateQueues();

   init_heap(50000);

   // 6) Call YOUR command handler -  interface method
   klogv(ANSI_COLOR_YELLOW "Transferring control to commhand..." ANSI_COLOR_RESET);

   PCB *commhand = load_proc("commhand", &comhand);
   PCB *idle_proc = load_proc("idle", &idle);
   // PCB *alarm = load_proc("alarm", &check_alarm);

   commhand->priority = 9;
   commhand->state = 0;
   commhand->suspend = 0;

   idle_proc->priority = 1;
   idle_proc->state = 0;
   idle_proc->suspend = 0;

   // alarm->priority = 8;
   // alarm->state = 1;
   // alarm->clas = 1;

   insertPCB(commhand);
   insertPCB(idle_proc);
   // insertPCB(alarm);

   if(gethours() < 4){
     sethours(gethours()+20);
   }
   else{
     sethours(gethours()-4);
   }
   setmin(getmins());
   setsec(getseconds());

   setday(getday());
   setmonth(getmonth());
   setyear(getyear());

   yield();

   // 7) System Shutdown on return from your command handler
   klogv(ANSI_COLOR_YELLOW "Starting system shutdown procedure..." ANSI_COLOR_RESET);

   /* Shutdown Procedure */
   klogv(ANSI_COLOR_GREEN "Shutdown complete. You may now turn off the machine. (QEMU: C-a x)" ANSI_COLOR_RESET);
   hlt();
}
