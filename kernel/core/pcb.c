#include "modules/mpx_supt.h"
#include  <string.h>
#include  "pcb.h"
#include "system.h"
#include <core/serial.h>

queue *readyQueue;
queue *blockedQueue;

PCB* allocatePCB(){
	struct PCB *process = sys_alloc_mem((sizeof(struct PCB)));
	if(process == NULL){
		char *msg = "Error: Memory Allocation Failed.";
		int len = strlen(msg);
		sys_req(WRITE,DEFAULT_DEVICE,msg,&len);
		return NULL;
	}
	// process->stack_top = pcb->stackBase+1024-sizeof(context);
	return process;
}
int allocateQueues(){
	blockedQueue = sys_alloc_mem(sizeof(struct queue));
	blockedQueue->count = 0;
	blockedQueue->head = NULL;
	blockedQueue->tail = NULL;
	if(blockedQueue == NULL){
		char *msg = "Error: Memory Allocation Failed.";
		int len = strlen(msg);
		sys_req(WRITE,DEFAULT_DEVICE,msg,&len);
		return -1;
	}
	readyQueue = sys_alloc_mem(sizeof(struct queue));
	readyQueue->count = 0;
	readyQueue->head = NULL;
	readyQueue->tail = NULL;
	if(readyQueue == NULL){
		char *msg = "Error: Memory Allocation Failed.";
		int len = strlen(msg);
		sys_req(WRITE,DEFAULT_DEVICE,msg,&len);
		return -1;
	}
	return 1;
}

int freePCB(PCB *pcb){
	int error;
	error = sys_free_mem((void *)pcb);
	//not sure about how to cast/use this
	//return -1 if nothing freed
	return error;
}

PCB* setupPCB(char *name,int class,int priority){
	PCB *process = allocatePCB();

	strcpy(process->name,name);
	process->priority = priority;
	process->clas = class;
	process->state = ready;
	process->suspend = unsuspended;
	process->stack_top = process->stack + 1024 - sizeof(context);

	return process;
}

void insertPCB(PCB *pcb){
	if(pcb->state == blocked){ //FIFO Queue

		if(blockedQueue->count == 0){ //empty
			blockedQueue->tail = pcb;
			blockedQueue->head = pcb;
			pcb->next = NULL;
			pcb->prev = NULL;

			blockedQueue->count +=1;
		}else{
			blockedQueue->tail->prev = pcb;
			pcb->next = blockedQueue->tail;
			pcb->prev = NULL;
			blockedQueue->tail = pcb;
			blockedQueue->count +=1;
		}
	}else {
		PCB *temp = readyQueue->head;

			if(readyQueue->count == 0){
				readyQueue->head = pcb;
				readyQueue->tail = pcb;
				pcb->next = NULL;
				pcb->prev = NULL;
				readyQueue->count +=1;

			}else if(readyQueue->count == 1){
				if(temp->priority < pcb->priority){
					pcb->prev = temp;
					pcb->next = NULL;
					temp->next = pcb;
					readyQueue->head = pcb;
				}else{
					pcb->next = temp;
					pcb->prev = NULL;
					temp->prev = pcb;
					readyQueue->tail = pcb;
				}
				readyQueue->count +=1;
			} else if(temp->priority < pcb->priority){
				pcb->prev = temp;
				pcb->next = NULL;
				temp->next = pcb;
				readyQueue->head = pcb;

				readyQueue->count += 1;
			} else if(readyQueue->tail->priority >= pcb->priority) {
				pcb->next = readyQueue->tail;
				readyQueue->tail->prev = pcb;
				readyQueue->tail = pcb;
				pcb->prev = NULL;

				readyQueue->count += 1;
			}
				else{
				int i = 0;
				while(i < readyQueue->count){
					if(pcb->priority > temp->priority){
						break;
					}
					if(temp->prev != NULL){
						temp = temp->prev;
					}
					i++;
				}
					PCB *p = temp->next;
					temp->next = pcb;
					pcb->prev = temp;
					pcb->next = p;
					p->prev = pcb;
			readyQueue->count += 1;
			}
	}
}

PCB* findPCB(char *processname){
	int i = 0;
	PCB *r = readyQueue->tail;
	while(i < readyQueue->count){
		if(strcmp(r->name,processname) == 0){
			return r;
		}
		if(r->next != NULL){
			r = r->next;
		}
		i++;
	}
	//not in ready queue
	i = 0;
	PCB *b = blockedQueue->tail;
	while(i < blockedQueue->count){
		if(strcmp(b->name,processname) == 0){
			return b;
		 }
		if(b->next != NULL){
			b = b->next;
		}
		i++;
	}//not in blocked queue

	return NULL;
}

int removePCB(PCB *pcb){
	if(pcb->state == ready){
		if(readyQueue->count == 1){
			readyQueue->head = NULL;
			readyQueue->tail = NULL;
			pcb->next=NULL;
			pcb->prev = NULL;
			readyQueue->count = 0;
			return 1;

		}else if(pcb->next == NULL){ //head
			PCB *temp = pcb->prev;
			pcb->prev = NULL;
			temp->next = NULL;
			readyQueue->head = temp;
			readyQueue->count -=1;
			return 1;

		}else if(pcb->prev == NULL){ //tail
			PCB *temp = pcb->next;
			pcb->next = NULL;
			temp->prev = NULL;
			readyQueue->tail = temp;
			readyQueue->count -=1;
			return 1;

		}else{
			PCB *p = pcb->prev;
			PCB *n = pcb->next;
			p->next = pcb->next;
			n->prev = p;
			pcb->prev = NULL;
			pcb->next = NULL;
			readyQueue->count -=1;
			return 1;

		}
	}else if(pcb->state == blocked){
		if(blockedQueue->count == 1){
			blockedQueue->head = NULL;
			blockedQueue->tail = NULL;
			pcb->next=NULL;
			pcb->prev = NULL;
			blockedQueue->count = 0;
			return 1;

		}else if(pcb->next == NULL){ //head
			PCB *temp = pcb->prev;
			pcb->prev = NULL;
			temp->next = NULL;
			blockedQueue->head = temp;
			blockedQueue->count -=1;
			return 1;

		}else if(pcb->prev == NULL){ //tail
			PCB *temp = pcb->next;
			temp->prev = NULL;
			readyQueue->head = temp;
			//pcb->next == NULL;
			blockedQueue->count -=1;
			return 1;

		}else{
			PCB *p = pcb->prev;
			PCB *n = pcb->next;
			p->next = pcb->next;
			n->prev = p;
			pcb->prev = NULL;
			pcb->next = NULL;
			blockedQueue->count -=1;
			return 1;
		}
	}else{
		//pcb not in a queue
		return 0;
	}
}

PCB* get_head() {
		return readyQueue->head;
}
