#include "comhand.h"
#include "modules/mpx_supt.h"
#include <string.h>
#include <stdint.h>
#include <string.h>
#include <core/io.h>
#include <core/serial.h>
// #include "pcb.h"
#include "pcbcommands.h"
#include "procsr3.h"
#include "mem_management.h"
#include "alarm.h"

#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_BLUE "\x1b[34m"

queue *readyQueue;

void comhand(){
	char *selections = ANSI_COLOR_CYAN "\n ~Welcome to S0S-MPX~ \n" ANSI_COLOR_RESET;

	int selectLen = strlen(selections);

	char cmdBuffer[100]; //buffer for command read in
	int bufferSize;
	int quit=0;

	// lets print this out one time only.. haha
	sys_req(WRITE,DEFAULT_DEVICE,selections,&selectLen);


	while(!quit) {
		char *prompt = ANSI_COLOR_CYAN "\n ツ > " ANSI_COLOR_RESET;
		int promptLen = strlen(prompt);

		sys_req(WRITE, DEFAULT_DEVICE, prompt, &promptLen);

		// get a command
		memset(cmdBuffer,'\0', 100);
		bufferSize = 99;

		sys_req(READ, DEFAULT_DEVICE, cmdBuffer, &bufferSize);
		sys_req(WRITE, DEFAULT_DEVICE, "\n", &bufferSize); //not sure how else to get a newline?

		if(strcmp(cmdBuffer, "quit") == 0) {
			quit = shutdown();
		}
		if (strcmp(cmdBuffer, "help") == 0){
			helplist();
		}
		if (strcmp(cmdBuffer, "date") == 0){
			getdate();
		}
	  if (strcmp(cmdBuffer, "time") == 0){
			gettime();
		}
		if (strcmp(cmdBuffer, "loadr3") == 0){
			loadr3();
		}
		if (strcmp(cmdBuffer, "yield") == 0){
			yield();
		}
		if (strcmp(cmdBuffer, "clear") == 0){
			clear();
		}

		// check if the command has any arguments
		int has_args = grab_or_check_args(cmdBuffer, bufferSize);

		if (has_args == 0) {
			int size = cmd_size(cmdBuffer, bufferSize);
			char cmd[size+1];

			// only grab the letters, no whitespace/null
			int i = 0;
			while (i < size) {
				cmd[i] = cmdBuffer[i];
				i++;
			}
			cmd[size] = '\0';

			if (strcmp(cmd, "version") == 0)
				version();
			else if (strcmp(cmd, "show_all") == 0)
				showAll();
			else if (strcmp(cmd, "infinite") == 0)
				start_infinite();
			else if (strcmp(cmd, "show_ready") == 0)
				showReady();
			else if (strcmp(cmd, "show_blocked") == 0)
				showBlocked();
			else if (strcmp(cmd, "show_allocated") == 0)
				show_list(1);
			else if (strcmp(cmd, "show_free") == 0)
				show_list(0);
			else if (strcmp(cmd, "is_mem_empty") == 0)
				check_mem();
			else
				print_error();

		} else if (has_args == 1) {
			grab_or_check_args(cmdBuffer, bufferSize);
		} else {
			print_error();
		}
		sys_req(IDLE, COM1, NULL, NULL);
	}


}

void print_error() {
	char *errormsg = ANSI_COLOR_RED "\nInvalid Command!\n" ANSI_COLOR_RESET;
	int erlen = strlen(errormsg);
	sys_req(WRITE,DEFAULT_DEVICE,errormsg,&erlen);
}

//prints current version to COM1
void version(){
	char *version = ANSI_COLOR_YELLOW "\nCurrent Version: R6 :( \nLast Updated: 04/30/2021\n" ANSI_COLOR_RESET;
	int strLen = strlen(version);
	sys_req(WRITE, DEFAULT_DEVICE, version, &strLen);
}


int shutdown(){
	// serial_print(ANSI_COLOR_RED "\nAre you sure you want to shut down [Y/N] \n" ANSI_COLOR_RESET);
	// char answer[4];
	// int len = strlen(answer);
	// memset(answer, '\0', 5);
	//
	// sys_req(READ,DEFAULT_DEVICE,answer,&len);
	// sys_req(WRITE, DEFAULT_DEVICE, answer, &len);
	// if(strcmp(answer, "yes") == 0) {
	// 	serial_print("\nfuck\n");
	// 	return 1;
	// }
	// serial_print("\nfuck\n");
	clearQueues();
	sys_req(EXIT, NULL, NULL, NULL);
	return 1;

}

//TODO: add other commands once implemented
void help(char command[]){
	if (strcmp(command, "version") == 0) {
		char *vers = ANSI_COLOR_YELLOW "\nDescription:\n displays current version of MPX system and completion date\n"
				"\nFlags: none\n" ANSI_COLOR_RESET;
		int vrslen = strlen(vers);
		sys_req(WRITE,DEFAULT_DEVICE, vers, &vrslen);
	} else if (strcmp(command,"date") == 0) {
			char *date1 = ANSI_COLOR_YELLOW"\nDescription:\n displays the system date.\n"
					"\nFlags: none\n" ANSI_COLOR_RESET;
			int datelen1 = strlen(date1);
			sys_req(WRITE,DEFAULT_DEVICE, date1, &datelen1);
	} else if (strcmp(command,"infinite") == 0) {
			char *date1 = ANSI_COLOR_YELLOW"\nDescription:\n runs the infinite process\n"
					"\nFlags: none\n" ANSI_COLOR_RESET;
			int datelen1 = strlen(date1);
			sys_req(WRITE,DEFAULT_DEVICE, date1, &datelen1);
	} else if (strcmp(command,"clear") == 0) {
			char *date5 = ANSI_COLOR_YELLOW"\nDescription:\n clears the terminal and places input at the top of the terminal window.\n"
					"\nFlags: none\n" ANSI_COLOR_RESET;
			int datelen5 = strlen(date5);
			sys_req(WRITE,DEFAULT_DEVICE, date5, &datelen5);
	} else if (strcmp(command,"time") == 0) {
	 			char *date2 = ANSI_COLOR_YELLOW"\nDescription:\n displays the system time.\n"
	 					"\nFlags: none\n" ANSI_COLOR_RESET;
	 			int datelen2 = strlen(date2);
	 			sys_req(WRITE,DEFAULT_DEVICE, date2, &datelen2);
	} else if (strcmp(command,"set_date") == 0) {
		char *date = ANSI_COLOR_YELLOW"\nDescription:\n sets the current system date.\n"
				"\nFlags: --set=\"value\": set system date\n" ANSI_COLOR_RESET;
		int datelen = strlen(date);
		sys_req(WRITE,DEFAULT_DEVICE, date, &datelen);
	} else if (strcmp(command, "set_time") == 0) {
		char *time = ANSI_COLOR_YELLOW"\nDescription:\n sets the current system time.\n"
				"\nFlags: --set=\"value\": set system time\n" ANSI_COLOR_RESET;
		int timelen = strlen(time);
		sys_req(WRITE, DEFAULT_DEVICE, time, &timelen);
	} else if (strcmp(command, "create_pcb") == 0) {
		char *create_pcb  = ANSI_COLOR_YELLOW"\nDescription:\n creates PCB with specified name, class, and priority.\n"
				"\nFlags: \n\t--name=\"value\": sets process name\n\t--class=\"value\": sets process class\n\t--priority=\"value\": sets process priority\n" ANSI_COLOR_RESET;

		int create_pcb_len = strlen(create_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, create_pcb, &create_pcb_len);
	} else if (strcmp(command, "delete_pcb") == 0) {
		char *delete_pcb  = ANSI_COLOR_YELLOW"\nDescription:\n deletes PCB with specified name.\n"
				"\nFlags: \n\t--name=\"value\": process name\n"ANSI_COLOR_RESET;

		int delete_pcb_len = strlen(delete_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, delete_pcb, &delete_pcb_len);
	} else if (strcmp(command, "block_pcb") == 0) {
		char *block_pcb  = ANSI_COLOR_YELLOW"\nDescription:\n places PCB with specified name in blocked state and reinserts into queue.\n"
				"\nFlags: \n\t--name=\"value\": process name\n"ANSI_COLOR_RESET;

		int block_pcb_len = strlen(block_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, block_pcb, &block_pcb_len);
	} else if (strcmp(command, "unblock_pcb") == 0) {
		char *unblock_pcb  = ANSI_COLOR_YELLOW"\nDescription:\n places PCB with specified name in unblocked state and reinserts into queue.\n"
				"\nFlags: \n\t--name=\"value\": process name\n"ANSI_COLOR_RESET;

		int unblock_pcb_len = strlen(unblock_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, unblock_pcb, &unblock_pcb_len);
	} else if (strcmp(command, "suspend_pcb") == 0) {
		char *suspend_pcb  = ANSI_COLOR_YELLOW "\nDescription:\n places PCB with specified name in suspended state and reinserts into queue.\n"
				"\nFlags: \n\t--name=\"value\": process name\n"ANSI_COLOR_RESET;

		int suspend_pcb_len = strlen(suspend_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, suspend_pcb, &suspend_pcb_len);
	} else if (strcmp(command, "resume_pcb") == 0) {
		char *resume_pcb  = ANSI_COLOR_YELLOW"\nDescription:\n places PCB with specified name in the not suspended state and reinserts into queue.\n"
				"\nFlags: \n\t--name=\"value\": process name\n"ANSI_COLOR_RESET;

		int resume_pcb_len = strlen(resume_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, resume_pcb, &resume_pcb_len);
	} else if (strcmp(command, "set_priority") == 0) {
		char *set_priority  = ANSI_COLOR_YELLOW"\nDescription:\n sets a PCB with a specific name to a specific priority.\n"
				"\nFlags: \n\t--name=\"value\": process name\n\t--priority=\"value\": new process priority\n"ANSI_COLOR_RESET;

		int set_priority_len = strlen(set_priority);
		sys_req(WRITE, DEFAULT_DEVICE, set_priority, &set_priority_len);
	} else if (strcmp(command, "show_pcb") == 0) {
		char *show_pcb  = ANSI_COLOR_YELLOW"\nDescription:\n displays the following information about the specified PCB: process name, class, state, suspended status, prioritys.\n"
				"\nFlags: \n\t--name=\"value\": process name\n"ANSI_COLOR_RESET;

		int show_pcb_len = strlen(show_pcb);
		sys_req(WRITE, DEFAULT_DEVICE, show_pcb, &show_pcb_len);
	} else if (strcmp(command, "show_ready") == 0) {
		char *show_ready  = ANSI_COLOR_YELLOW"\nDescription:\n displays the following information about PCBs in the ready queue: process name, class, state, suspended status, prioritys.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int show_ready_len = strlen(show_ready);
		sys_req(WRITE, DEFAULT_DEVICE, show_ready, &show_ready_len);
	} else if (strcmp(command, "show_blocked") == 0) {
		char *show_blocked  = ANSI_COLOR_YELLOW "\nDescription:\n displays the following information about PCBs in the blocked queue: process name, class, state, suspended status, prioritys.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int show_blocked_len = strlen(show_blocked);
		sys_req(WRITE, DEFAULT_DEVICE, show_blocked, &show_blocked_len);
	} else if (strcmp(command, "show_all") == 0) {
		char *show_all  = ANSI_COLOR_YELLOW"\nDescription:\n displays the following information about PCBs in all queues: process name, class, state, suspended status, prioritys.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int show_all_len = strlen(show_all);
		sys_req(WRITE, DEFAULT_DEVICE, show_all, &show_all_len);
	} else if (strcmp(command, "create_alarm") == 0) {
		char *create_alarm = ANSI_COLOR_YELLOW"\nDescription:\n creates an alarm with a message to go off at a certain time.\n"
				"\nFlags: \n\t--m=\"value\": message\n\t--t=\"value\": time (hr:min)\n"ANSI_COLOR_RESET;

		int create_alarm_len = strlen(create_alarm);
		sys_req(WRITE, DEFAULT_DEVICE, create_alarm, &create_alarm_len);
	}
	else if (strcmp(command, "yield") == 0) {
		char *yield = ANSI_COLOR_YELLOW"\nDescription:\n yields the processor to the operating system.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int yield_len = strlen(yield);
		sys_req(WRITE, DEFAULT_DEVICE, yield, &yield_len);
	}
	else if (strcmp(command, "loadr3") == 0) {
		char *loadr3 = ANSI_COLOR_YELLOW"\nDescription:\n loads the test processes for the R3 module.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int loadr3_len = strlen(loadr3);
		sys_req(WRITE, DEFAULT_DEVICE, loadr3, &loadr3_len);
	}
	else if (strcmp(command, "show_allocated") == 0) {
		char *show_allocated = ANSI_COLOR_YELLOW"\nDescription:\n shows the current allocated blocks of memory.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int show_allocated_len = strlen(show_allocated);
		sys_req(WRITE, DEFAULT_DEVICE, show_allocated, &show_allocated_len);
	}else if (strcmp(command, "show_free") == 0) {
		char *show_free = ANSI_COLOR_YELLOW"\nDescription:\n shows the current free blocks of memory.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int show_free_len = strlen(show_free);
		sys_req(WRITE, DEFAULT_DEVICE, show_free, &show_free_len);
	}  else if (strcmp(command, "allocate_memory") == 0) {
		char *allocate_memory = ANSI_COLOR_YELLOW"\nDescription:\n creates a block of allocated memory from the intial heap.\n"
				"\nFlags: \n\t--size=\"value\": the size of the memory you want to assign.\n"ANSI_COLOR_RESET;

		int allocate_memory_len = strlen(allocate_memory);
		sys_req(WRITE, DEFAULT_DEVICE, allocate_memory, &allocate_memory_len);
	}  else if (strcmp(command, "free_memory") == 0) {
		char *free_memory = ANSI_COLOR_YELLOW"\nDescription:\n frees a block of allocated memory in the heap.\n"
				"\nFlags: \n\t--addr=\"value\": the adress of the allocated memory block.\n"ANSI_COLOR_RESET;

		int free_memory_len = strlen(free_memory);
		sys_req(WRITE, DEFAULT_DEVICE, free_memory, &free_memory_len);
	}  else if (strcmp(command, "is_mem_empty") == 0) {
		char *is_mem_empty = ANSI_COLOR_YELLOW"\nDescription:\n checks if there is any memory allocated.\n"
				"\nFlags: none\n"ANSI_COLOR_RESET;

		int empty_len = strlen(is_mem_empty);
		sys_req(WRITE, DEFAULT_DEVICE, is_mem_empty, &empty_len);
	}   else {
		print_error();
	}
}


int grab_or_check_args(char cmdBuffer[], int bufferSize) {
	// need to make copy or else strtok() function changes cmdBuffer[]
	char cmdBuffer_copy[bufferSize];
	strcpy(cmdBuffer_copy, cmdBuffer);

	int splitCmdLen = strlen(cmdBuffer);
	int cmd_len = cmd_size(cmdBuffer, bufferSize);

	char *split_cmd[4];
	char *checkToken = strtok(cmdBuffer_copy, "--");


	int i = 0;
	// split string by delimeter
	while (checkToken != NULL) {
		split_cmd[i++] = checkToken;
		checkToken = strtok(NULL, "--");
	}

	// if (strcmp(split_cmd[0], "create_alarm") == 0) {
	// 	char *message = parse_argument(split_cmd[1], "=\"");
	// 	char *time = parse_argument(split_cmd[2], "=\"");
	// 	insert_alarm(message, time);
	// }

	char *splitCmd[splitCmdLen];
	char *cmdToken = strtok(cmdBuffer, " --");

	// fill array with null values
	memset(splitCmd, '\0', splitCmdLen);

	i = 0;
	// split string by delimeter
	while (cmdToken != NULL) {
		splitCmd[i++] = cmdToken;
		cmdToken = strtok(NULL, " --");
	}

	// check if args exists
	if (splitCmd[1] == NULL) {
		return 0;
	} else {
		int arg_size = strlen(splitCmd[1]);
		int c_size = strlen(splitCmd[0])+1;

		char arg[arg_size+1];
		char command[bufferSize+1];

		strcpy(arg, splitCmd[1]);
		strcpy(command, splitCmd[0]);

		arg[arg_size] = '\0';
		command[c_size] = '\0';

		char *help_cmds[] = {"help", "h"};

		// check if the command matches with help
		if ((strcmp(arg, help_cmds[0]) == 0) || (strcmp(arg, help_cmds[1]) == 0)) {
			if (strcmp(command, "version") == 0) {
			// check if no extra spaces/etc for version
				if (cmd_len == 11 || cmd_len == 14) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "date") == 0) {
				if (cmd_len == 8 || cmd_len == 11) {
					help(command);
				} else {
					return 2;
				}
			}else if (strcmp(command, "infinite") == 0) {
					if (cmd_len == 12 || cmd_len == 15) {
						help(command);
					} else {
						return 2;
					}
			} else if (strcmp(command, "clear") == 0) {
				if (cmd_len == 9 || cmd_len == 12) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "time") == 0) {
				if (cmd_len == 8 || cmd_len == 11) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "set_date") == 0) {
				if (cmd_len == 12 || cmd_len == 15) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "set_time") == 0) {
				if (cmd_len == 12 || cmd_len == 15) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "create_pcb") == 0) {
				if (cmd_len == 14 || cmd_len == 17) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "delete_pcb") == 0) {
				if (cmd_len == 14 || cmd_len == 17) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "block_pcb") == 0) {
				if (cmd_len == 13 || cmd_len == 16) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "unblock_pcb") == 0) {
				if (cmd_len == 15 || cmd_len == 18) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "suspend_pcb") == 0) {
				if (cmd_len == 15 || cmd_len == 18) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "resume_pcb") == 0) {
				if (cmd_len == 14 || cmd_len == 17) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "set_priority") == 0) {
				if (cmd_len == 16 || cmd_len == 19) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "show_pcb") == 0) {
				if (cmd_len == 12 || cmd_len == 15) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "show_ready") == 0) {
				if (cmd_len == 14 || cmd_len == 17) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "show_blocked") == 0) {
				if (cmd_len == 16 || cmd_len == 19) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "show_all") == 0) {
				if (cmd_len == 12 || cmd_len == 15) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "create_alarm") == 0) {
				if (cmd_len == 16 || cmd_len == 19) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "init_heap") == 0) {
				if (cmd_len == 13 || cmd_len == 16) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "allocate_memory") == 0) {
				if (cmd_len == 19 || cmd_len == 22) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "free_memory") == 0) {
				if (cmd_len == 15 || cmd_len == 18) {
					help(command);
				} else {
					return 2;
				}
			} else if (strcmp(command, "is_mem_empty") == 0) {
				if (cmd_len == 16 || cmd_len == 19) {
					help(command);
				} else {
					return 2;
				}
			} else {
				return 2;
			}
		} else {
			if (strcmp(command, "create_alarm") == 0){
				char *message = parse_argument(split_cmd[1], "=\"");
				char *time = parse_argument(split_cmd[2], "=\"");

				char *ptr = strtok(time, ":");
				char *date[3];
				int i = 0;
				while (ptr != NULL)
				{
					date[i] = ptr;
					ptr = strtok(NULL, ":");
					i++;
				}
				set_alarm(message, atoi(date[0]), atoi(date[1]), atoi(date[2]));

			}
			else if (strcmp(command, "set_date") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");
				char *ptr = strtok(name, "/");
				char *date[3];
				int i = 0;
				while (ptr != NULL)
				{
					date[i] = ptr;
					ptr = strtok(NULL, "/");
					i++;
				}
				setdate(atoi(date[0]), atoi(date[1]), atoi(date[2]));

			} else if (strcmp(command, "set_time") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");
				char *ptr = strtok(name, ":");
				char *date[3];
				int i = 0;
				while (ptr != NULL)
				{
					date[i] = ptr;
					ptr = strtok(NULL, ":");
					i++;
				}
				settime(atoi(date[0]), atoi(date[1]), atoi(date[2]));

			} else if (strcmp(command, "create_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");
				int class = atoi(parse_argument(splitCmd[2], "class=\""));
				int priority = atoi(parse_argument(splitCmd[3], "priority=\""));

				createPCB(name, class, priority);
			} else if (strcmp(command, "delete_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");

				deletePCB(name);
			} else if (strcmp(command, "block_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");

				blockPCB(name);
			} else if (strcmp(command, "unblock_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");

				unblockPCB(name);
			} else if (strcmp(command, "suspend_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");

				suspendPCB(name);
			} else if (strcmp(command, "resume_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");

				resumePCB(name);
			} else if (strcmp(command, "set_priority") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");
				int priority = atoi(parse_argument(splitCmd[2], "priority=\""));

				setPCBpriority(name, priority);
			} else if (strcmp(command, "show_pcb") == 0) {
				char *name = parse_argument(splitCmd[1], "=\"");
				showPCB(name);
			} else if (strcmp(command, "init_heap") == 0) {
				//error check one day
				int size = atoi(parse_argument(splitCmd[1], "=\""));
				init_heap(size);

				char *msg = "\n Heap initialized to a size of ";
				int msg_size = strlen(msg);
				sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);

				char size_m[msg_size];
				itoa(size, size_m);
				msg_size = strlen(size_m);
				sys_req(WRITE, DEFAULT_DEVICE, size_m, &msg_size);

				char *carriage = "\n";
				msg_size = strlen(carriage);
				sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);
			} else if (strcmp(command, "allocate_memory") == 0) {
				//error check one day
				int size = atoi(parse_argument(splitCmd[1], "=\""));
				allocate_memory(size);

				char *msg = "\n Memory allocated with size of ";
				int msg_size = strlen(msg);
				sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);

				char size_m[msg_size];
				itoa(size, size_m);
				msg_size = strlen(size_m);
				sys_req(WRITE, DEFAULT_DEVICE, size_m, &msg_size);

				char *carriage = "\n";
				msg_size = strlen(carriage);
				sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);
			} else if (strcmp(command, "free_memory") == 0) {
				int addr = atoi(parse_argument(splitCmd[1], "=\""));
				u32int status = free_memory((u32int) addr);

				if (status != NULL) {
					char *msg = "\n Memory freed at address ";
					int msg_size = strlen(msg);
					sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);

					char size_m[msg_size];
					itoa(addr, size_m);
					msg_size = strlen(size_m);
					sys_req(WRITE, DEFAULT_DEVICE, size_m, &msg_size);

					char *carriage = "\n";
					msg_size = strlen(carriage);
					sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);
				}
			} else if (strcmp(command, "grab_info") == 0) {
				int addr = atoi(parse_argument(splitCmd[1], "=\""));
				grab_info((u32int) addr);
			}
		}
	}

	return 1;
}

char * parse_argument(char arg01[], char delimiter[]) {
	char *split_arg[2];

	char *s_token = strtok(arg01, delimiter);

	int i = 0;
	// split string by delimeter
	while (s_token != NULL) {
		// sys_req(WRITE, )
		split_arg[i++] = s_token;
		s_token = strtok(NULL, delimiter);
	}

	if(strcmp(delimiter, "class=\"") == 0 || strcmp(delimiter, "priority=\"") == 0)
		return split_arg[0];
	else
		return split_arg[1];

}

int cmd_size(char cmdBuffer[], int bufferSize) {
	int i = 0;
	int size = 0;
	while (i < bufferSize) {
		if (cmdBuffer[i] != '\0') {
			size++;
		}
		i++;
	}

	return size;
}

void helplist(){
	char *msg = ANSI_COLOR_CYAN "\n |-------- The following list is all of the available functions for the MPX operating system --------|"
							"\n |-------- For functions with paramters the following format can be used: function_name --parameter=\"value\" --------|\n" ANSI_COLOR_RESET;
	int msg_size = strlen(msg);
	sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);
	char *msg1 = ANSI_COLOR_CYAN "\n--- version ---\n" ANSI_COLOR_RESET;
	int msg_size1 = strlen(msg1);
	sys_req(WRITE, DEFAULT_DEVICE, msg1, &msg_size1);
	help("version");
	char *msg91 = ANSI_COLOR_CYAN "\n--- infinite ---\n" ANSI_COLOR_RESET;
	int msg_size91 = strlen(msg91);
	sys_req(WRITE, DEFAULT_DEVICE, msg91, &msg_size91);
	help("infinite");
	char *msg53 = ANSI_COLOR_CYAN "\n--- clear ---\n" ANSI_COLOR_RESET;
	int msg_size53 = strlen(msg53);
	sys_req(WRITE, DEFAULT_DEVICE, msg53, &msg_size53);
	help("clear");
	char *msg30 = ANSI_COLOR_CYAN "\n--- date ---\n" ANSI_COLOR_RESET;
	int msg_size30 = strlen(msg30);
	sys_req(WRITE, DEFAULT_DEVICE, msg30, &msg_size30);
	help("date");
	char *msg31 = ANSI_COLOR_CYAN "\n--- time ---\n" ANSI_COLOR_RESET;
	int msg_size31 = strlen(msg31);
	sys_req(WRITE, DEFAULT_DEVICE, msg31, &msg_size31);
	help("time");
	char *msg2 = ANSI_COLOR_CYAN "\n--- set_date ---\n" ANSI_COLOR_RESET;
	int msg_size2 = strlen(msg2);
	sys_req(WRITE, DEFAULT_DEVICE, msg2, &msg_size2);
	help("set_date");
	char *msg3 = ANSI_COLOR_CYAN"\n--- set_time ---\n" ANSI_COLOR_RESET;
	int msg_size3 = strlen(msg3);
	sys_req(WRITE, DEFAULT_DEVICE, msg3, &msg_size3);
	help("set_time");
	char *msg11 = ANSI_COLOR_CYAN"\n--- create_pcb ---\n" ANSI_COLOR_RESET;
	int msg_size11 = strlen(msg11);
	sys_req(WRITE, DEFAULT_DEVICE, msg11, &msg_size11);
	help("create_pcb");
	char *msg12 = ANSI_COLOR_CYAN"\n--- delete_pcb---\n" ANSI_COLOR_RESET;
	int msg_size12 = strlen(msg12);
	sys_req(WRITE, DEFAULT_DEVICE, msg12, &msg_size12);
	help("delete_pcb");
	char *msg13 = ANSI_COLOR_CYAN"\n--- block_pcb ---\n" ANSI_COLOR_RESET;
	int msg_size13 = strlen(msg13);
	sys_req(WRITE, DEFAULT_DEVICE, msg13, &msg_size13);
	help("block_pcb");
	char *msg14 = ANSI_COLOR_CYAN"\n--- unblock_pcb ---\n" ANSI_COLOR_RESET;
	int msg_size14 = strlen(msg14);
	sys_req(WRITE, DEFAULT_DEVICE, msg14, &msg_size14);
	help("unblock_pcb");
	char *msg15 = ANSI_COLOR_CYAN"\n--- suspend_pcb ---\n" ANSI_COLOR_RESET;
	int msg_size15 = strlen(msg15);
	sys_req(WRITE, DEFAULT_DEVICE, msg15, &msg_size15);
	help("suspend_pcb");
	char *msg16 = ANSI_COLOR_CYAN"\n--- resume_pcb ---\n" ANSI_COLOR_RESET;
	int msg_size16 = strlen(msg16);
	sys_req(WRITE, DEFAULT_DEVICE, msg16, &msg_size16);
	help("resume_pcb");
	char *msg17 = ANSI_COLOR_CYAN"\n--- set_priority ---\n" ANSI_COLOR_RESET;
	int msg_size17 = strlen(msg17);
	sys_req(WRITE, DEFAULT_DEVICE, msg17, &msg_size17);
	help("set_priority");
	char *msg4 = ANSI_COLOR_CYAN"\n--- show_all ---\n" ANSI_COLOR_RESET;
	int msg_size4 = strlen(msg4);
	sys_req(WRITE, DEFAULT_DEVICE, msg4, &msg_size4);
	help("show_all");
	char *msg5 = ANSI_COLOR_CYAN"\n--- show_ready ---\n" ANSI_COLOR_RESET;
	int msg_size5 = strlen(msg5);
	sys_req(WRITE, DEFAULT_DEVICE, msg5, &msg_size5);
	help("show_ready");
	char *msg6 = ANSI_COLOR_CYAN"\n--- show_blocked ---\n" ANSI_COLOR_RESET;
	int msg_size6 = strlen(msg6);
	sys_req(WRITE, DEFAULT_DEVICE, msg6, &msg_size6);
	help("show_blocked");
	char *msg7 = ANSI_COLOR_CYAN"\n--- yield ---\n" ANSI_COLOR_RESET;
	int msg_size7 = strlen(msg7);
	sys_req(WRITE, DEFAULT_DEVICE, msg7, &msg_size7);
	help("yield");
	char *msg8 = ANSI_COLOR_CYAN"\n--- loadr3 ---\n" ANSI_COLOR_RESET;
	int msg_size8 = strlen(msg8);
	sys_req(WRITE, DEFAULT_DEVICE, msg8, &msg_size8);
	help("loadr3");
	char *msg18 = ANSI_COLOR_CYAN"\n--- create_alarm ---\n" ANSI_COLOR_RESET;
	int msg_size18 = strlen(msg18);
	sys_req(WRITE, DEFAULT_DEVICE, msg18, &msg_size18);
	help("create_alarm");
	char *msg9 = ANSI_COLOR_CYAN"\n--- show_allocated ---\n" ANSI_COLOR_RESET;
	int msg_size9 = strlen(msg9);
	sys_req(WRITE, DEFAULT_DEVICE, msg9, &msg_size9);
	help("show_allocated");
	char *msg10 = ANSI_COLOR_CYAN"\n--- show_free ---\n" ANSI_COLOR_RESET;
	int msg_size10 = strlen(msg10);
	sys_req(WRITE, DEFAULT_DEVICE, msg10, &msg_size10);
	help("show_free");
	char *msg19 = ANSI_COLOR_CYAN"\n--- allocate_memory ---\n" ANSI_COLOR_RESET;
	int msg_size19 = strlen(msg19);
	sys_req(WRITE, DEFAULT_DEVICE, msg19, &msg_size19);
	help("allocate_memory");
	char *msg20 = ANSI_COLOR_CYAN"\n--- free_memory ---\n" ANSI_COLOR_RESET;
	int msg_size20 = strlen(msg20);
	sys_req(WRITE, DEFAULT_DEVICE, msg20, &msg_size20);
	help("free_memory");
	char *msg21 = ANSI_COLOR_CYAN"\n--- is_mem_empty ---\n" ANSI_COLOR_RESET;
	int msg_size21 = strlen(msg21);
	sys_req(WRITE, DEFAULT_DEVICE, msg21, &msg_size21);
	help("is_mem_empty");
  }

void getdate(){
	char fulldate[22];
	int year,month,day;
	char years[5];
	year = getyear();
	itoa(year,years);

	char months[5];
	month = getmonth();
	itoa(month,months);

	char days[5];
	day = getday();
	itoa(day,days);

	strcat(fulldate, ANSI_COLOR_CYAN "\n Current Date: " ANSI_COLOR_RESET);
	strcat(fulldate,months);
	strcat(fulldate,"/");
	strcat(fulldate, days);
	strcat(fulldate, "/");
	strcat(fulldate,years);
	int size = strlen(fulldate);

	sys_req(WRITE,DEFAULT_DEVICE,fulldate,&size);

	char *carriage = "\n";
	int msg_size = strlen(carriage);
	sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);
}

void setdate(int month, int day, int year){
	cli();
	setyear(year);
	setmonth(month);
	setday(day);
	sti();
}

void gettime(){
	char fulldate[18];
	int hour,minute,second;
	char hours[3];
	hour = gethours();
	itoa(hour,hours);

	char minutes[3];
	minute = getmins();
	itoa(minute,minutes);

	char seconds[3];
	second = getseconds();
	itoa(second,seconds);

  strcat(fulldate, ANSI_COLOR_CYAN "\n Current Time: " ANSI_COLOR_RESET);
	strcat(fulldate, hours);
	strcat(fulldate,":");
	strcat(fulldate, minutes);
	strcat(fulldate, ":");
	strcat(fulldate,seconds);
	strcat(fulldate,"\n");
	int size = strlen(fulldate);

	sys_req(WRITE,DEFAULT_DEVICE,fulldate,&size);

}

void settime(int hours, int minutes, int seconds){
	cli();
	sethours(hours);
	setmin(minutes);
	setsec(seconds);
	sti();
}

int getyear(){
	int year;
	outb(0X70,0x09);
	unsigned char years = inb(0x71);
	year = BCDconvert(years);

	return year;
}

void setyear(int year){
	int bcd;
	bcd = intToBCD(year);
	//cli();
	outb(0x70,0x09);
	outb(0x71,bcd);
	//sti();
}
int getmonth(){
	int month;
	outb(0X70,0x08);
	unsigned char months = inb(0x71);
	month = BCDconvert(months);

	return month;
}
void setmonth(int month){
	int bcd;
	bcd = intToBCD(month);
	//cli();
	outb(0x70,0x08);
	outb(0x71,bcd);
	//sti();
}
int getday(){
	int day;
	outb(0X70,0x07);
	unsigned char days = inb(0x71);
	day = BCDconvert(days);

	return day;
}
void setday(int day){
    unsigned char bcd;
	bcd = intToBCD(day);
	//cli();
	outb(0x70,0x07);
	outb(0x71,bcd);
	//sti();
}
int gethours(){
	int hour;
	outb(0X70,0x04);
	unsigned char hours = inb(0x71);
	hour = BCDconvert(hours);
	return hour;
}

void sethours(int hour){
	int bcd;
	bcd = intToBCD(hour);
	// cli();
	outb(0x70,0x04);
	outb(0x71,bcd);
	// sti();
}
int getmins(){
	int min;
	outb(0X70,0x02);
	int minutes = inb(0x71);
	min = BCDconvert(minutes);

	return min;
}

void setmin(int min){
	int bcd;
	bcd = intToBCD(min);
	// cli();
	outb(0x70,0x02);
	outb(0x71,bcd);
	// sti();
}
int getseconds(){
	int secs;
	outb(0X70,0x00);
	unsigned char seconds = inb(0x71);
	secs = BCDconvert(seconds);

	return secs;
}

void setsec(int seconds){
	int bcd;
	bcd = intToBCD(seconds);
	// cli();
	outb(0x70,0x00);
	outb(0x71,bcd);
	// sti();
	}

int BCDconvert(unsigned char time){
	return time-6 * (time>>4);
}
int intToBCD(int time){
	return (((time/10)<<4) | (time%10));
}

char* itoa(int number, char* buffer) {
	int index = 0;
    if (number == 0){
        buffer[index] = '0';
        index++;
        buffer[index] = '\0';
        return buffer;
    }
    while (number != 0)
    {
        int remainder = number % 10;
        if(remainder > 9){
        	buffer[index++]= (remainder-10)+'a';
        }else{
        	buffer[index++]=remainder + '0';
        }
        number = number/10;
    }

    buffer[index] = '\0';
    reverse(buffer);

    return buffer;
}
void reverse(char input[]){
	if (*input == 0) {
	    return;
	 }
	 char *front = input;  //index to front of str
	 char *end = front + strlen(input) - 1; //get last character not '\0'
	 char temp;
	 while (end > front){ //reverse order
		 temp = *front;
	     *front = *end;
	     *end = temp;

	     front++;
	     end--;
	    }
}

void yield() {
	asm volatile("int $60");
}

PCB *load_proc(char *name, void (*func)(void)) {
	PCB *new_pcb = setupPCB(name, 0, 1);
	new_pcb->suspend = 1;
	new_pcb->state = 0;
	context *cp = (context*)(new_pcb->stack_top);
	memset(cp, 0, sizeof(context));
	cp->fs = 0x10;
	cp->gs = 0x10;
	cp->ds = 0x10;
	cp->es = 0x10;
	cp->cs = 0x8;
	cp->ebp = (u32int)(new_pcb->stack);
	cp->esp = (u32int)(new_pcb->stack_top);
	cp->eip = (u32int) func;
	cp->eflags = 0x202;
	return new_pcb;
}

void loadr3() {
	int size = 32;
	insertPCB(load_proc("proc1", &proc1));
	sys_req(WRITE, DEFAULT_DEVICE, ANSI_COLOR_GREEN "Process 1 loaded!\n" ANSI_COLOR_RESET, &size);
	insertPCB(load_proc("proc2", &proc2));
	sys_req(WRITE, DEFAULT_DEVICE, ANSI_COLOR_GREEN "Process 2 loaded!\n" ANSI_COLOR_RESET, &size);
	insertPCB(load_proc("proc3", &proc3));
	sys_req(WRITE, DEFAULT_DEVICE, ANSI_COLOR_GREEN "Process 3 loaded!\n" ANSI_COLOR_RESET, &size);
	insertPCB(load_proc("proc4", &proc4));
	sys_req(WRITE, DEFAULT_DEVICE, ANSI_COLOR_GREEN "Process 4 loaded!\n" ANSI_COLOR_RESET, &size);
	insertPCB(load_proc("proc5", &proc5));
	sys_req(WRITE, DEFAULT_DEVICE, ANSI_COLOR_GREEN "Process 5 loaded!\n" ANSI_COLOR_RESET, &size);
}

void start_idle() {
	idle();
}

void start_infinite(){
	infinite();
}

// void insert_alarm(char *message, int hours, int minutes, int seconds) {
//
// }

// void check_alarm() {
//
// 	PCB *z = findPCB("alarm_");
// 	while (z != NULL) {
// 		if (z->msg != NULL) {
// 				char *parsedTime[2];
// 				int x = 0;
// 				char *s_token = strtok(z->time, ":");
//
// 				while (s_token != NULL) {
// 					parsedTime[x++] = s_token;
// 					s_token = strtok(NULL, ":");
// 				}
//
// 				// int parsedTimeSize = strlen(parsedTime[0]);
//
// 				// sys_req(WRITE, DEFAULT_DEVICE, "test\n", &parsedTimeSize);
//
// 				// sys_req(WRITE, DEFAULT_DEVICE, parsedTime[0], &parsedTimeSize);
//
// 				int hours = atoi(parsedTime[0]);
// 				int minutes = atoi(parsedTime[1]);
//
// 				if (hours == gethours() && minutes == getmins()) {
// 					char *message = z->msg;
// 					int msg_size = strlen(z->msg);
// 					char *hours = NULL;
// 					char *mins = NULL;
//
// 					itoa(gethours(), hours);
// 					itoa(getmins(), mins);
// 					sys_req(WRITE, DEFAULT_DEVICE, message, &msg_size);
// 					break;
// 				}
// 			} else {
// 				if(z->next != NULL) {
// 					z = z->next;
// 				}
// 			}
// 	}
// }

void check_mem() {
	int status = is_empty();

	if (status == 0) {
		char *msg = "\n There is allocated memory! ";
		int msg_size = strlen(msg);
		sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);
	} else if (status == 1) {
		char *msg = "\n There is no allocated memory!";
		int msg_size = strlen(msg);
		sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);
	}
}

void clear(){
	  char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J";
		int msg_size = strlen(CLEAR_SCREEN_ANSI);
	  sys_req(WRITE, DEFAULT_DEVICE, CLEAR_SCREEN_ANSI, &msg_size);
}
