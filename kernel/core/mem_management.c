#include <string.h>
#include <core/serial.h>
#include <mem/heap.h>
#include "modules/mpx_supt.h"
#include "comhand.h"
#include "mem_management.h"

list allocated_list;
list free_list;

u32int memory_start;

void init_heap(int size) {
  memory_start = kmalloc(size + sizeof(CMCB) + sizeof(LMCB));

  CMCB *first_cmcb = (CMCB*) memory_start;
  first_cmcb->type = free;
  first_cmcb->size = size;
  first_cmcb->start_address = (int) memory_start + sizeof(CMCB);

  LMCB *first_lmcb = (LMCB*) (memory_start + sizeof(CMCB) + size);
  first_lmcb->type = free;
  first_lmcb->size = size;
  first_cmcb->lmcb_pointy = first_lmcb;

  free_list.head = first_cmcb;
  free_list.head->next_cmcb = NULL;
  free_list.head->prev_cmcb = NULL;

  allocated_list.head = NULL;
  allocated_list.head->next_cmcb = NULL;
  allocated_list.head->prev_cmcb = NULL;
}

u32int allocate_memory(int size) {
  CMCB *curr = free_list.head;

  u32int req_size = size + sizeof(CMCB) + sizeof(LMCB);
  u32int curr_size;

  /*
  * start from the beginning of the list and try to find a block big enough,
  * grab the size if so
  */
  while (curr) {
    if (curr->size >= req_size) {
      curr_size = curr->size;
      break;
    }
    curr = curr->next_cmcb;
  }

  int curr_type = curr->type;
  u32int curr_lmcb = (u32int) curr->lmcb_pointy;
  int curr_next = (int) curr->next_cmcb;
  int curr_prev = (int) curr->prev_cmcb;

  /*
   * create allocated memory control block
   */
  CMCB *amcb_block = curr;
  amcb_block->type = allocated;
  amcb_block->size = size;
  amcb_block->start_address = (u32int) curr + sizeof(CMCB);

  /*
  * create limited memory control block
  */
  LMCB *lmcb_block = (LMCB*) (amcb_block->start_address + size);
  lmcb_block->type = allocated;
  lmcb_block->size = size;

  /*
   * link the cmcb with its corersponding lmcb
   */
  amcb_block->lmcb_pointy = (LMCB*) lmcb_block;

  /*
   * create a free memory block from leftover memory after allocation
   */
  CMCB *fmcb_block = (CMCB*) ((u32int)lmcb_block + sizeof(LMCB));
  fmcb_block->type = curr_type;
  fmcb_block->size = curr_size - size - sizeof(CMCB) - sizeof(LMCB);
  fmcb_block->start_address = ((u32int) lmcb_block + sizeof(LMCB)) + sizeof(CMCB);
  fmcb_block->lmcb_pointy = (LMCB*) curr_lmcb;
  fmcb_block->next_cmcb = (CMCB*) curr_next;
  fmcb_block->prev_cmcb = (CMCB*) curr_prev;

  /*
  * insert into the allocated/free list in correct order
  */
  if (free_list.head == curr) {
    free_list.head = fmcb_block;
  }

  if(allocated_list.head == NULL) {
    allocated_list.head = amcb_block;
  } else {
    CMCB *pointy = allocated_list.head;
    // find spot in allocated/free list that can fit
    while (pointy < amcb_block && pointy->next_cmcb != NULL) {
      pointy = pointy->next_cmcb;
    }

    // check where to insert the memory block
    if (pointy < amcb_block) {
      pointy->next_cmcb = amcb_block;
      amcb_block->prev_cmcb = pointy;
    } else {
      amcb_block->next_cmcb = pointy;
      amcb_block->prev_cmcb = pointy->prev_cmcb;
      pointy->prev_cmcb = amcb_block;

      // link previous cmcb to current amcb_block if not head node
      if (amcb_block->prev_cmcb != NULL) {
        amcb_block->prev_cmcb->next_cmcb = amcb_block;
      }
    }
  }

  CMCB *copy_of_amcb = amcb_block;
  copy_of_amcb->type = amcb_block->type;

  return (u32int) amcb_block->start_address;
}

void grab_info(u32int address) {
  CMCB *curr = free_list.head;
  int found = 0;
  while (curr != NULL) {
    if(curr->start_address == address) {
      char *starting_address_addr = "\nstarting address: ";
      int msg_size = strlen(starting_address_addr);
      sys_req(WRITE, DEFAULT_DEVICE, starting_address_addr, &msg_size);

      // char starting_addr[32];
      // itoa(curr->start_address, starting_addr);
      // msg_size = strlen(starting_addr);
      // sys_req(WRITE, DEFAULT_DEVICE, starting_addr, &msg_size);
      //
      // char *next_block_msg = "\nnext block type: ";
      // msg_size = strlen(next_block_msg);
      // sys_req(WRITE, DEFAULT_DEVICE, next_block_msg, &msg_size);
      //
      // char next_block_type[32];
      // itoa(curr->next_cmcb->type, next_block_type);
      // msg_size = strlen(next_block_type);
      // sys_req(WRITE, DEFAULT_DEVICE, next_block_type, &msg_size);
      //
      // char *next_block_addr_msg = "\nnext block addr: ";
      // msg_size = strlen(next_block_addr_msg);
      // sys_req(WRITE, DEFAULT_DEVICE, next_block_addr_msg, &msg_size);
      //
      // char next_block_addr[32];
      // itoa(curr->next_cmcb->start_address, next_block_addr);
      // msg_size = strlen(next_block_addr);
      // sys_req(WRITE, DEFAULT_DEVICE, next_block_addr, &msg_size);
      //
      // char *prev_block_msg = "\nprevious block type:  ";
      // msg_size = strlen(prev_block_msg);
      // sys_req(WRITE, DEFAULT_DEVICE, prev_block_msg, &msg_size);
      //
      // char prev_block_type[32];
      // itoa(curr->prev_cmcb->type, prev_block_type);
      // msg_size = strlen(next_block_type);
      // sys_req(WRITE, DEFAULT_DEVICE, next_block_type, &msg_size);
      //
      // char *prev_block_addr_msg = "\nprevious block addr:  ";
      // msg_size = strlen(prev_block_addr_msg);
      // sys_req(WRITE, DEFAULT_DEVICE, prev_block_addr_msg, &msg_size);
      //
      // char prev_block_addr[32];
      // itoa(curr->prev_cmcb->start_address, prev_block_addr);
      // msg_size = strlen(prev_block_addr);
      // sys_req(WRITE, DEFAULT_DEVICE, prev_block_addr, &msg_size);

      char *cmcb_msg = "\n CMCB size: ";
      msg_size = strlen(cmcb_msg);
      sys_req(WRITE, DEFAULT_DEVICE, cmcb_msg, &msg_size);

      char cmcb_block[32];
      itoa(sizeof(CMCB), cmcb_block);
      msg_size = strlen(cmcb_block);
      sys_req(WRITE, DEFAULT_DEVICE, cmcb_block, &msg_size);

      char *size_msg = "\n size: ";
      msg_size = strlen(size_msg);
      sys_req(WRITE, DEFAULT_DEVICE, size_msg, &msg_size);

      char size[32];
      itoa(curr->size, size);
      msg_size = strlen(size);
      sys_req(WRITE, DEFAULT_DEVICE, size, &msg_size);

      char *lmcb_msg = "\n LMCB size: ";
      msg_size = strlen(lmcb_msg);
      sys_req(WRITE, DEFAULT_DEVICE, lmcb_msg, &msg_size);

      char lmcb_block[32];
      itoa(sizeof(LMCB), lmcb_block);
      msg_size = strlen(lmcb_block);
      sys_req(WRITE, DEFAULT_DEVICE, lmcb_block, &msg_size);

      char *lmcb_msg_1 = "\n pointer LMCB size: ";
      msg_size = strlen(lmcb_msg_1);
      sys_req(WRITE, DEFAULT_DEVICE, lmcb_msg_1, &msg_size);

      char lmcb_block_1[32];
      itoa(curr->lmcb_pointy->size, lmcb_block_1);
      msg_size = strlen(lmcb_block_1);
      sys_req(WRITE, DEFAULT_DEVICE, lmcb_block_1, &msg_size);

      char *carriage = "\n";
      msg_size = strlen(carriage);
      sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);

      found = 1;
    }
    curr = curr->next_cmcb;
  }

  if (!found) {
    curr = allocated_list.head;

    while (curr != NULL) {
      if(curr->start_address == address) {
        // char *starting_address_addr = "\nstarting address: ";
        // int msg_size = strlen(starting_address_addr);
        // sys_req(WRITE, DEFAULT_DEVICE, starting_address_addr, &msg_size);
        //
        // char starting_addr[32];
        // itoa(curr->start_address, starting_addr);
        // msg_size = strlen(starting_addr);
        // sys_req(WRITE, DEFAULT_DEVICE, starting_addr, &msg_size);
        //
        // char *next_block_msg = "\nnext block type: ";
        // msg_size = strlen(next_block_msg);
        // sys_req(WRITE, DEFAULT_DEVICE, next_block_msg, &msg_size);
        //
        // char next_block_type[32];
        // itoa(curr->next_cmcb->type, next_block_type);
        // msg_size = strlen(next_block_type);
        // sys_req(WRITE, DEFAULT_DEVICE, next_block_type, &msg_size);
        //
        // char *next_block_addr_msg = "\nnext block addr: ";
        // msg_size = strlen(next_block_addr_msg);
        // sys_req(WRITE, DEFAULT_DEVICE, next_block_addr_msg, &msg_size);
        //
        // char next_block_addr[32];
        // itoa(curr->next_cmcb->start_address, next_block_addr);
        // msg_size = strlen(next_block_addr);
        // sys_req(WRITE, DEFAULT_DEVICE, next_block_addr, &msg_size);
        //
        // char *prev_block_msg = "\nprevious block type:  ";
        // msg_size = strlen(prev_block_msg);
        // sys_req(WRITE, DEFAULT_DEVICE, prev_block_msg, &msg_size);
        //
        // char prev_block_type[32];
        // itoa(curr->prev_cmcb->type, prev_block_type);
        // msg_size = strlen(next_block_type);
        // sys_req(WRITE, DEFAULT_DEVICE, next_block_type, &msg_size);
        //
        // char *prev_block_addr_msg = "\nprevious block addr:  ";
        // msg_size = strlen(prev_block_addr_msg);
        // sys_req(WRITE, DEFAULT_DEVICE, prev_block_addr_msg, &msg_size);
        //
        // char prev_block_addr[32];
        // itoa(curr->prev_cmcb->start_address, prev_block_addr);
        // msg_size = strlen(prev_block_addr);
        // sys_req(WRITE, DEFAULT_DEVICE, prev_block_addr, &msg_size);


        char *cmcb_msg = "\n CMCB size: ";
        int msg_size = strlen(cmcb_msg);
        sys_req(WRITE, DEFAULT_DEVICE, cmcb_msg, &msg_size);

        char cmcb_block[32];
        itoa(sizeof(CMCB), cmcb_block);
        msg_size = strlen(cmcb_block);
        sys_req(WRITE, DEFAULT_DEVICE, cmcb_block, &msg_size);

        char *size_msg = "\n size: ";
        msg_size = strlen(size_msg);
        sys_req(WRITE, DEFAULT_DEVICE, size_msg, &msg_size);

        char size[32];
        itoa(curr->size, size);
        msg_size = strlen(size);
        sys_req(WRITE, DEFAULT_DEVICE, size, &msg_size);

        char *lmcb_msg = "\n LMCB size ";
        msg_size = strlen(lmcb_msg);
        sys_req(WRITE, DEFAULT_DEVICE, lmcb_msg, &msg_size);

        char lmcb_block[32];
        itoa(sizeof(LMCB), lmcb_block);
        msg_size = strlen(lmcb_block);
        sys_req(WRITE, DEFAULT_DEVICE, lmcb_block, &msg_size);

        char *lmcb_msg_1 = "\n pointer LMCB size: ";
        msg_size = strlen(lmcb_msg_1);
        sys_req(WRITE, DEFAULT_DEVICE, lmcb_msg_1, &msg_size);

        char lmcb_block_1[32];
        itoa(curr->lmcb_pointy->size, lmcb_block_1);
        msg_size = strlen(lmcb_block_1);
        sys_req(WRITE, DEFAULT_DEVICE, lmcb_block_1, &msg_size);

        char *carriage = "\n";
        msg_size = strlen(carriage);
        sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);
      }
      curr = curr->next_cmcb;
    }
  }
}

u32int free_memory(u32int address) {
  int block_exists = 0;
  CMCB *allocated_pointy = allocated_list.head;
  CMCB *free_pointy = free_list.head;

  /*
   * loop through the allocated list
   */
  while(allocated_pointy != NULL) {
    if (allocated_pointy->start_address == address) {

      // set both cmcb and lmcb to free
      allocated_pointy->type = free;
      allocated_pointy->lmcb_pointy->type = free;

      // check start of list first
      if (allocated_pointy == allocated_list.head) {
        allocated_list.head = allocated_pointy->next_cmcb;
        allocated_pointy->next_cmcb->prev_cmcb = NULL;
      } else {
        if (allocated_pointy->next_cmcb != NULL) {
          allocated_pointy->prev_cmcb->next_cmcb = allocated_pointy->next_cmcb;
          allocated_pointy->next_cmcb->prev_cmcb = allocated_pointy->prev_cmcb;
        } else {
          allocated_pointy->prev_cmcb->next_cmcb = NULL;
        }
      }

      allocated_pointy->prev_cmcb = NULL;
      allocated_pointy->next_cmcb = NULL;

      // set the new head for the free_list
      if (free_list.head->start_address > allocated_pointy->start_address) {
        free_list.head->prev_cmcb = allocated_pointy;
        allocated_pointy->next_cmcb = free_list.head;
        free_list.head = allocated_pointy;
        allocated_pointy->prev_cmcb = NULL;
      } else {
        // traverse through the free list
        while (free_pointy->start_address < allocated_pointy->start_address && free_pointy->next_cmcb != NULL) {
          free_pointy = free_pointy->next_cmcb;
        }

        // check where to place the free block
        if(free_pointy->start_address < allocated_pointy->start_address && free_pointy->next_cmcb == NULL) {
          //after
          free_pointy->next_cmcb = allocated_pointy;
          allocated_pointy->prev_cmcb = free_pointy;
          allocated_pointy->next_cmcb = NULL;
        } else {
          //before
          allocated_pointy->next_cmcb = free_pointy;
          allocated_pointy->prev_cmcb = free_pointy->prev_cmcb;
          free_pointy->prev_cmcb = allocated_pointy;
          allocated_pointy->prev_cmcb->next_cmcb = allocated_pointy;
        }
      }

      if(allocated_pointy->next_cmcb != NULL){
         CMCB *nxt = allocated_pointy->next_cmcb;

         if((allocated_pointy->start_address + allocated_pointy->size + sizeof(LMCB) + sizeof(CMCB)) == nxt->start_address){
         allocated_pointy->size = allocated_pointy->size + nxt->size + sizeof(LMCB) + sizeof(CMCB);
         allocated_pointy->next_cmcb = nxt->next_cmcb;
         nxt->next_cmcb->prev_cmcb = allocated_pointy;
        // nxt->prev_cmcb = NULL;
         //nxt->next_cmcb = NULL;
        }
      }

      //merge adjacent free memory
      if(allocated_pointy->prev_cmcb != NULL){
         CMCB *prev = allocated_pointy->prev_cmcb;
        if(allocated_pointy->start_address - sizeof(CMCB) - sizeof(LMCB) - prev->size == prev->start_address){
             prev->size = prev->size + allocated_pointy->size + sizeof(LMCB) + sizeof(CMCB);
             prev->next_cmcb = allocated_pointy->next_cmcb;
             allocated_pointy->next_cmcb->prev_cmcb = prev;
             allocated_pointy->next_cmcb = NULL;
             allocated_pointy->prev_cmcb = NULL;
             allocated_pointy->start_address = prev->start_address;
             allocated_pointy = (CMCB*) prev;

             if(prev == free_list.head){
               free_list.head = allocated_pointy;
             }
             //allocated_pointy->size = allocated_pointy->size + prev->size;
             //  allocated_pointy->prev_cmcb = prev->prev_cmcb;
            // prev->prev_cmcb->next_cmcb = allocated_pointy;
            // allocated_pointy->start_address = prev->start_address;
            // prev->prev_cmcb = NULL;
             //prev->next_cmcb = NULL;
           }
      }

      block_exists = 1;
      break;
    }
    allocated_pointy = allocated_pointy->next_cmcb;
  }

  if(block_exists == 0) {
    char *msg = "\nMemory block doesn't exist!\n ";
    int msg_size = strlen(msg);
    sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);
    return NULL;
  }

  return address;
}

int is_empty() {
  return allocated_list.head == NULL;
}

void show_list(int type) {
  CMCB* pointy;

  if(type == 1) {
    pointy = allocated_list.head;
  } else if(type == 0) {
    pointy = free_list.head;
  }

  if(pointy == NULL) {
    char *msg;
    if(type == 0) {
      msg = "\nThere's nothing in the free memory list!\n";
    } else if(type == 1) {
      msg = "\nThere's nothing in the allocated memory list!\n";
    }
    int msg_size = strlen(msg);
    sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);
  } else {
    while(pointy != NULL) {
      char *msg = "\nsize: ";
      int msg_size = strlen(msg);
      sys_req(WRITE, DEFAULT_DEVICE, msg, &msg_size);

      char block_size[32];
      itoa(pointy->size, block_size);
      int block_msg_size = strlen(block_size);
      sys_req(WRITE, DEFAULT_DEVICE, block_size, &block_msg_size);

      char *addr_msg = "\naddress: ";
      msg_size = strlen(msg);
      sys_req(WRITE, DEFAULT_DEVICE, addr_msg, &msg_size);

      char block_addr[32];
      itoa(pointy->start_address, block_addr);
      int block_addr_size = strlen(block_addr);
      sys_req(WRITE, DEFAULT_DEVICE, block_addr, &block_addr_size);

      char *carriage = "\n";
      msg_size = strlen(carriage);
      sys_req(WRITE, DEFAULT_DEVICE, carriage, &msg_size);

      pointy = pointy->next_cmcb;
    }
  }
}
