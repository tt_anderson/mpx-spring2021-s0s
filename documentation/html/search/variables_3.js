var searchData=
[
  ['cdir_615',['cdir',['../paging_8c.html#af7da380833e92d5c7d3c3db41484713b',1,'paging.c']]],
  ['clas_616',['clas',['../struct_p_c_b.html#aadd5019d1a62df0ef35fa760ce1a1d82',1,'PCB']]],
  ['cop_617',['cop',['../system_8c.html#a199c6145b9b5ee43b3961894ee22830a',1,'system.c']]],
  ['count_618',['count',['../structqueue.html#ac71ad15edf9cf721b2fdb85524c51bf0',1,'queue']]],
  ['count_5fptr_619',['count_ptr',['../structparam.html#ab61d8fe96dbeed1ef0387802ac453c53',1,'param']]],
  ['cs_620',['cs',['../struct_context.html#a3cc18b6d282bcebf13327adb750a56e5',1,'Context']]],
  ['curr_5fheap_621',['curr_heap',['../heap_8c.html#afaac4d3fb801ecbd3c6fe3c995d5cf82',1,'heap.c']]],
  ['current_5fmodule_622',['current_module',['../mpx__supt_8c.html#a3d19c725b7f9f45e9da97a79ca6a4737',1,'mpx_supt.c']]]
];
