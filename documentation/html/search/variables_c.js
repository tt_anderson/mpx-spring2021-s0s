var searchData=
[
  ['max_5fsize_669',['max_size',['../structheap.html#ad2e0262828735d6e437facbfce37d6b0',1,'heap']]],
  ['mem_5fsize_670',['mem_size',['../paging_8c.html#abf8475f59bfb67fac4b6b5a254dfe56d',1,'paging.c']]],
  ['memory_5fstart_671',['memory_start',['../mem__management_8c.html#aa1323e5ce305cc57f099390215e0c3de',1,'mem_management.c']]],
  ['min_672',['min',['../structdate__time.html#af93fdd2e01117a0171a2583718166d2a',1,'date_time']]],
  ['min_5fsize_673',['min_size',['../structheap.html#a7b4422774c5ca7ac8ed5ddfe95f5c8ec',1,'heap']]],
  ['mon_674',['mon',['../structdate__time.html#a6e8a5baa74a619330ba9925cf0baf250',1,'date_time']]],
  ['msg_675',['msg',['../struct_p_c_b.html#a93023c366081d063bfbefcfaa04e02c9',1,'PCB']]],
  ['msg1_676',['msg1',['../procsr3_8c.html#a2564e18bd45c394fec8c69b3ad9b2bc7',1,'procsr3.c']]],
  ['msg2_677',['msg2',['../procsr3_8c.html#a7a69aa7c74529e544e6259fe1e2a91f6',1,'procsr3.c']]],
  ['msg3_678',['msg3',['../procsr3_8c.html#a0bd7065816fcf679e26ad9a7f67014f3',1,'procsr3.c']]],
  ['msg4_679',['msg4',['../procsr3_8c.html#a0aadbd881daa7338dac73e94119613e1',1,'procsr3.c']]],
  ['msg5_680',['msg5',['../procsr3_8c.html#aaf062c405d30e9c34f261f66dbd0adec',1,'procsr3.c']]],
  ['msgsize_681',['msgSize',['../procsr3_8c.html#acafd8e3e9ef0f4c5e7e90fb05aeab3af',1,'procsr3.c']]]
];
