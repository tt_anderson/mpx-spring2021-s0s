var searchData=
[
  ['flags_648',['flags',['../structidt__entry__struct.html#a46c92bd8f07d5ff4e379a07b293c46af',1,'idt_entry_struct::flags()'],['../structgdt__entry__struct.html#afac75bdf53080168c8899c442862410a',1,'gdt_entry_struct::flags()'],['../tables_8h.html#a138dda98fcd4738346af61bcca8cf4b4',1,'flags():&#160;tables.h']]],
  ['frameaddr_649',['frameaddr',['../structpage__entry.html#a68a6dc54a7ab6f7fb1a068476190bf67',1,'page_entry']]],
  ['frames_650',['frames',['../paging_8c.html#a76492529572a1a20a06076ac40d66b29',1,'paging.c']]],
  ['free_5flist_651',['free_list',['../mem__management_8c.html#ac36981027daca22a419776abee8be65a',1,'mem_management.c']]],
  ['fs_652',['fs',['../struct_context.html#afb78d7fa76f272032362d6b18a59a01b',1,'Context']]]
];
