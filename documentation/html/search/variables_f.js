var searchData=
[
  ['page_5fsize_687',['page_size',['../paging_8c.html#a1cc472551ec40b65eef931fde01054e7',1,'paging.c']]],
  ['pages_688',['pages',['../structpage__table.html#aa066e0fa847ce2fafb6a2feddfa340ff',1,'page_table']]],
  ['params_689',['params',['../system_8c.html#a3b4b77494d0fad58939896ddc5290c99',1,'params():&#160;system.c'],['../mpx__supt_8c.html#a3b4b77494d0fad58939896ddc5290c99',1,'params():&#160;mpx_supt.c']]],
  ['phys_5falloc_5faddr_690',['phys_alloc_addr',['../heap_8c.html#a6dfa4ef84e115e891b3679e4932b5c49',1,'phys_alloc_addr():&#160;heap.c'],['../paging_8c.html#a6dfa4ef84e115e891b3679e4932b5c49',1,'phys_alloc_addr():&#160;heap.c']]],
  ['present_691',['present',['../structpage__entry.html#a34148a94af9bfabbb8c4f00f9865dfee',1,'page_entry']]],
  ['prev_692',['prev',['../struct_p_c_b.html#a3086bfad0a32b9c3457747bc88feba8a',1,'PCB']]],
  ['prev_5fcmcb_693',['prev_cmcb',['../struct_c_m_c_b.html#a316ad449266760fa17c3edaf0766c267',1,'CMCB']]],
  ['priority_694',['priority',['../struct_p_c_b.html#a84a8d888cc661a040f4f1a081807d5b4',1,'PCB']]]
];
