var searchData=
[
  ['sec_697',['sec',['../structdate__time.html#ac43a109ccc7f3c46840afa8a30dc51f1',1,'date_time']]],
  ['serial_5fport_5fin_698',['serial_port_in',['../serial_8c.html#a1a756238531fc5bf1096f89dc18e835e',1,'serial.c']]],
  ['serial_5fport_5fout_699',['serial_port_out',['../serial_8c.html#adbb2c18b0aaab5c1927a6f674768a710',1,'serial.c']]],
  ['size_700',['size',['../structheader.html#af8cc659f702446226bc2ebabba437d5d',1,'header::size()'],['../structindex__entry.html#a2b0247aae5c7f9884f8eef1ee121adb0',1,'index_entry::size()'],['../struct_c_m_c_b.html#a6a59762bf6da8d8f1c9bb408a1c1c412',1,'CMCB::size()'],['../struct_l_m_c_b.html#a10c88a922fd0715001e042e4c7fc6891',1,'LMCB::size()']]],
  ['sselect_701',['sselect',['../structidt__entry__struct.html#a85254c7df6a612f4a4b3bb470ff3370c',1,'idt_entry_struct::sselect()'],['../tables_8h.html#ab3f34507900160b4a9b309b4ed039e07',1,'sselect():&#160;tables.h']]],
  ['stack_702',['stack',['../struct_p_c_b.html#ad16b941b22b8d62d7b63c7d4ca15210d',1,'PCB']]],
  ['stack_5fbase_703',['stack_base',['../struct_p_c_b.html#ac839636883d797b777355667d7ddc262',1,'PCB']]],
  ['stack_5ftop_704',['stack_top',['../struct_p_c_b.html#a8227c3083af92819c8531e78e6451e3a',1,'PCB']]],
  ['start_5faddress_705',['start_address',['../struct_c_m_c_b.html#ae7c229d869115e6ada7185afa377d3ee',1,'CMCB']]],
  ['state_706',['state',['../struct_p_c_b.html#ab78c42c991b73e180b38488358fb3511',1,'PCB']]],
  ['student_5ffree_707',['student_free',['../mpx__supt_8c.html#a19c47c02b0338bc13716d98305bb8a34',1,'mpx_supt.c']]],
  ['student_5fmalloc_708',['student_malloc',['../mpx__supt_8c.html#a421e2b48efb5facc71d16979252710e2',1,'mpx_supt.c']]],
  ['suspend_709',['suspend',['../struct_p_c_b.html#aa7101095660f1d3d70558da8fd04f1cf',1,'PCB']]]
];
