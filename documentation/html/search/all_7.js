var searchData=
[
  ['idt_5fentry_5fstruct_43',['idt_entry_struct',['../structidt__entry__struct.html',1,'']]],
  ['idt_5fstruct_44',['idt_struct',['../structidt__struct.html',1,'']]],
  ['index_5fentry_45',['index_entry',['../structindex__entry.html',1,'']]],
  ['index_5ftable_46',['index_table',['../structindex__table.html',1,'']]],
  ['init_5fheap_47',['init_heap',['../mem__management_8h.html#a0704ea50195bdbeebe60b36bc07b9074',1,'mem_management.c']]],
  ['initalarm_48',['initAlarm',['../alarm_8h.html#a4a4dd484a8a782e1fc21fa513b797243',1,'alarm.c']]],
  ['insert_5falarm_49',['insert_alarm',['../comhand_8h.html#a172bae54d85b9d5025714d6acdf07894',1,'comhand.h']]],
  ['insertpcb_50',['insertPCB',['../pcb_8h.html#a0eca4c150214eca5ee0cee9e5bc04660',1,'pcb.c']]],
  ['introduction_51',['Introduction',['../index.html',1,'']]],
  ['inttobcd_52',['intToBCD',['../comhand_8h.html#aa6d74fa841e40821f8508614805cedc7',1,'comhand.c']]],
  ['io_53',['io',['../structio.html',1,'']]],
  ['iocb_54',['IOCB',['../_m_p_x___r6_8h.html#a569f9ed1af0534975a107628a68474e4',1,'MPX_R6.h']]],
  ['is_5fempty_55',['is_empty',['../mem__management_8h.html#aa77441abb51d931eda08806ba657ddfe',1,'mem_management.c']]],
  ['itoa_56',['itoa',['../comhand_8h.html#a840e88f275c63e224be62a7038012387',1,'comhand.h']]]
];
